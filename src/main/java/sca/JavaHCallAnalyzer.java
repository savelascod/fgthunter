package sca;

import fileio.FileHExplorer;
import org.xml.sax.SAXException;
import parsers.SRCMLHParser;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JavaHCallAnalyzer {

    public static Map<String, List<String>> visitCallNodes(String projectDirPath, String folderPath, String projectId, String javaCode) throws IOException, InterruptedException, ParserConfigurationException, SAXException {
        Map<String, List<String>> calledMethods = new HashMap<>();
        List<String> calls = SRCMLHParser.getCallGraph(javaCode);
        for (String call : calls) {
            List<String> classes = FileHExplorer.searchClassesContainingMethodName(folderPath, projectId, call);
            if (classes.size() > 0) {
                for (String classResult : classes) {
                    String classPath = projectDirPath + System.getProperty("file.separator") + classResult.substring(1, classResult.length() - 1);
                    List<String> method = SRCMLHParser.getClassMethodBySignature(classPath, call);
                    if (!method.isEmpty()) {
                        calledMethods.put(classPath, method);
                    }
                }
            }
        }
        return calledMethods;
    }

}
