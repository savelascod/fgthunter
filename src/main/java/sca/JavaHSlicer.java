package sca;

import org.xml.sax.SAXException;
import parsers.SRCMLHParser;
import utils.TextUtils;

import javax.xml.parsers.ParserConfigurationException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class JavaHSlicer {

    public static String getSlice(String javaCode, List<String> criteria) throws IOException, InterruptedException, ParserConfigurationException, SAXException {
        String srcmlFilePath = SRCMLHParser.generateSRCMLFromJavaCode(javaCode);
        /*
        String srcSliceResult = "ResetPasswordAction.java,makeEmailApp,email,def{195,205},use{196,197,198,199,201,206,207,208,209,211},dvars{},pointers{},cfuncs{}\n" +
                "ResetPasswordAction.java,makeEmailApp,p,def{194,204},use{197,199,207,209},dvars{},pointers{},cfuncs{format{1},asList{0}}\n" +
                "ResetPasswordAction.java,makeEmailApp,role,def{191},use{193},dvars{},pointers{},cfuncs{}\n" +
                "ResetPasswordAction.java,makeEmailApp,eemid,def{191},use{},dvars{},pointers{},cfuncs{}\n" +
                "ResetPasswordAction.java,getSecurityQuestion,mid,def{121},use{123,126},dvars{},pointers{},cfuncs{}\n" +
                "ResetPasswordAction.java,resetPassword,r,def{149},use{156,158},dvars{},pointers{},cfuncs{}\n" +
                "ResetPasswordAction.java,resetPassword,ipAddr,def{147},use{161,174},dvars{},pointers{},cfuncs{recordResetPasswordFailure{1}}\n" +
                "ResetPasswordAction.java,resetPassword,confirmPassword,def{147},use{166,223,228},dvars{},pointers{},cfuncs{validatePassword{2}}\n" +
                "ResetPasswordAction.java,resetPassword,emid,def{146},use{},dvars{},pointers{},cfuncs{}\n" +
                "ResetPasswordAction.java,resetPassword,password,def{146},use{147,166,169,223,225,228,230},dvars{},pointers{},cfuncs{resetPassword{2},validatePassword{2},validatePassword{1}}\n" +
                "ResetPasswordAction.java,resetPassword,answer,def{146},use{168},dvars{},pointers{},cfuncs{}\n" +
                "ResetPasswordAction.java,resetPassword,role,def{146},use{151,158,170,191,193},dvars{},pointers{},cfuncs{makeEmailApp{2},parse{1}}\n" +
                "ResetPasswordAction.java,checkAnswerNull,eeanswer,def{107},use{},dvars{},pointers{},cfuncs{}\n" +
                "ResetPasswordAction.java,isMaxedOut,eeipAddress,def{67},use{},dvars{},pointers{},cfuncs{}\n" +
                "ResetPasswordAction.java,checkRole,role,def{81},use{83,84,85,86,87,88,89},dvars{},pointers{},cfuncs{}\n" +
                "ResetPasswordAction.java,checkRole,mid,def{81},use{83,84,85,86,87,88},dvars{},pointers{},cfuncs{}\n" +
                "ResetPasswordAction.java,checkMID,mid,def{49},use{50,52},dvars{},pointers{},cfuncs{}\n" +
                "ResetPasswordAction.java,checkMID,midString,def{47},use{49},dvars{},pointers{},cfuncs{valueOf{1}}\n" +
                "ResetPasswordAction.java,validatePassword,errorList,def{224},use{226,229,231,235},dvars{},pointers{},cfuncs{}\n" +
                "ResetPasswordAction.java,validatePassword,confirmPassword,def{223},use{228},dvars{},pointers{},cfuncs{}\n" +
                "ResetPasswordAction.java,validatePassword,password,def{223},use{225,228,230},dvars{},pointers{},cfuncs{}\n" +
                "ResetPasswordAction.java,ResetPasswordAction1,this,def{36,37,38},use{},dvars{},pointers{},cfuncs{}\n" +
                "ResetPasswordAction.java,ResetPasswordAction1,factory,def{35},use{36,37,38},dvars{this},pointers{},cfuncs{}";
                */
        String srcSliceResult = callSlicer(srcmlFilePath);

        List<String> srcSlices = Arrays.asList(srcSliceResult.split("\n"));
        ArrayList<String> locs = new ArrayList<>();

        for (String criterion : criteria) {
                String sliceLOC = getDependentLOC(srcSlices, criterion);
                for (String loc : sliceLOC.split(";")) {
                    if (!loc.isEmpty()) {
                        locs.add(loc);
                    }
                }
        }
        Collections.sort(locs);
        return SRCMLHParser.getLinesOfcode(srcmlFilePath, locs);
    }

    public static String callSlicer(String srcmlFilePath) throws IOException, InterruptedException {
        Runtime runtime = Runtime.getRuntime();
        Process process = runtime.exec("srcslice " + srcmlFilePath);
        BufferedReader inputReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
        String consoleOutput;
        String srcmlSliceResult = "";
        while ((consoleOutput = inputReader.readLine()) != null) {
            srcmlSliceResult += consoleOutput + "\n";
        }
        process.waitFor(5, TimeUnit.MINUTES);
        process.destroy();
        return srcmlSliceResult;
    }

    public static String getDependentLOC(List<String> srcSliceOut, String criterion) {
        //Set<String> sliceLOC = new LinkedHashSet<>();
        String sliceLOC = "";
        for (String srcSlice : srcSliceOut) {
            List<String> slice = TextUtils.splitByCommas(srcSlice);
            if (!slice.isEmpty()) {
                if (slice.get(2).contains(criterion)) {
                        for (String loc : TextUtils.splitByCommas(TextUtils.getContentOfCurvyBrackets(slice.get(3)))) {
                            sliceLOC += loc + ";";
                        }
                        for (String loc : TextUtils.splitByCommas(TextUtils.getContentOfCurvyBrackets(slice.get(4)))) {
                            sliceLOC += loc + ";";
                        }


                    for (String variable : TextUtils.splitByCommas(TextUtils.getContentOfCurvyBrackets(slice.get(5)))) {
                        List<String> srcSliceOutProcessed = new ArrayList<>();
                        srcSliceOutProcessed.addAll(srcSliceOutProcessed);
                        srcSliceOutProcessed.remove(srcSlice);
                        sliceLOC += getDependentLOC(srcSliceOutProcessed, variable);
                    }
                }
            }
        }
        return sliceLOC;
    }

}
