package parsers;

import com.github.javaparser.JavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.ImportDeclaration;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.NodeList;
import com.github.javaparser.ast.body.*;
import com.github.javaparser.ast.expr.MarkerAnnotationExpr;
import com.github.javaparser.ast.stmt.*;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.nio.charset.StandardCharsets;
import java.util.List;

import static utils.TextUtils.formatStringWithENDL;

/**
 * Created by mordreth on 3/20/18.
 */
public class JavaHParser {

    private static CompilationUnit getCompilationUnitFromClassPath(String classPath) throws FileNotFoundException {
        FileInputStream inputClassFile = new FileInputStream(classPath);
        CompilationUnit cu;
        try {
            cu = JavaParser.parse(inputClassFile, StandardCharsets.UTF_8);
        } catch (Exception e) {
            cu = JavaParser.parse(inputClassFile, StandardCharsets.ISO_8859_1);
        }
        return cu;
    }

    public static String getStatementListSpeechText(NodeList<Statement> statements) {
        String speech = "";
        for (Statement statement : statements) {
            if (statement instanceof ExpressionStmt) {
                ExpressionStmt expressionStmt = statement.asExpressionStmt();
                speech += formatStringWithENDL(expressionStmt.toString());
            } else if (statement instanceof IfStmt) {
                IfStmt ifStmt = statement.asIfStmt();
                speech += formatStringWithENDL(ifStmt.getCondition().toString());
                NodeList<Statement> thenStatements = new NodeList<>();
                thenStatements.add(ifStmt.getThenStmt());
                speech += getStatementListSpeechText(thenStatements);
                if (ifStmt.getElseStmt().isPresent()) {
                    NodeList<Statement> elseStatements = new NodeList<>();
                    elseStatements.add(ifStmt.getElseStmt().get());
                    speech += getStatementListSpeechText(elseStatements);
                }
            } else if (statement instanceof WhileStmt) {
                WhileStmt whileStmt = statement.asWhileStmt();
                speech += formatStringWithENDL(whileStmt.getCondition().toString());
                NodeList<Statement> whileStatements = new NodeList<>();
                whileStatements.add(whileStmt.getBody());
                speech += getStatementListSpeechText(whileStatements);
            } else if (statement instanceof ForStmt) {
                ForStmt forStmt = statement.asForStmt();
                NodeList<Statement> forStatements = new NodeList<>();
                forStatements.add(forStmt.getBody());
                speech += getStatementListSpeechText(forStatements);
            } else if (statement instanceof DoStmt) {
                DoStmt doStmt = statement.asDoStmt();
                speech += formatStringWithENDL(doStmt.getCondition().toString());
                NodeList<Statement> doStatements = new NodeList<>();
                doStatements.add(doStmt.getBody());
                speech += getStatementListSpeechText(doStatements);
            } else if (statement instanceof ForeachStmt) {
                ForeachStmt foreachStmt = statement.asForeachStmt();
                NodeList<Statement> foreachStatements = new NodeList<>();
                foreachStatements.add(foreachStmt.getBody());
                speech += getStatementListSpeechText(foreachStatements);
            } else if (statement instanceof SwitchStmt) {
                SwitchStmt switchStmt = statement.asSwitchStmt();
                speech += formatStringWithENDL(switchStmt.getSelector().toString());
                for (SwitchEntryStmt switchEntry : switchStmt.getEntries()) {
                    speech += getStatementListSpeechText(switchEntry.getStatements());
                }
            } else if (statement instanceof LocalClassDeclarationStmt) {
                LocalClassDeclarationStmt localClassDeclarationStmt = statement.asLocalClassDeclarationStmt();
                speech += getSpeechTextFromNodes(localClassDeclarationStmt.getChildNodes());
            } else if (statement instanceof TryStmt) {
                TryStmt tryStmt = statement.asTryStmt();
                speech += getStatementListSpeechText(tryStmt.getTryBlock().getStatements());
                if (tryStmt.getFinallyBlock().isPresent()) {
                    NodeList<Statement> finallyBlockStatements = new NodeList<>();
                    finallyBlockStatements.add(tryStmt.getFinallyBlock().get());
                    speech += getStatementListSpeechText(finallyBlockStatements);
                }
                for (CatchClause catchClause : tryStmt.getCatchClauses()) {
                    speech += formatStringWithENDL(catchClause.getParameter().getNameAsString());
                    speech += getStatementListSpeechText(catchClause.getBody().getStatements());
                }
            } else if (statement instanceof ReturnStmt) {
                ReturnStmt returnStmt = statement.asReturnStmt();
                speech += formatStringWithENDL(returnStmt.getExpression().toString());
            } else if (statement instanceof ThrowStmt) {
                ThrowStmt throwStmt = statement.asThrowStmt();
                speech += formatStringWithENDL(throwStmt.getExpression().toString());
            } else if (statement instanceof BlockStmt) {
                BlockStmt blockStmt = statement.asBlockStmt();
                speech += getStatementListSpeechText(blockStmt.getStatements());
            }
        }
        return speech;
    }

    public static String getSpeechTextFromNodes(List<Node> nodes) {
        String speech = "";
        for (Node node : nodes) {
            if (node instanceof AnnotationDeclaration) {
                AnnotationDeclaration annotationDeclaration = (AnnotationDeclaration) node;
                speech += getSpeechTextFromNodes(annotationDeclaration.getChildNodes());
            } else if (node instanceof AnnotationMemberDeclaration) {
                AnnotationMemberDeclaration annotationMemberDeclaration = (AnnotationMemberDeclaration) node;
                speech += formatStringWithENDL(annotationMemberDeclaration.toString());
            } else if (node instanceof ClassOrInterfaceDeclaration) {
                ClassOrInterfaceDeclaration classOrInterfaceDeclaration = (ClassOrInterfaceDeclaration) node;
                speech += getSpeechTextFromNodes(classOrInterfaceDeclaration.getChildNodes());
            } else if (node instanceof FieldDeclaration) {
                FieldDeclaration fieldDeclaration = (FieldDeclaration) node;
                speech += formatStringWithENDL(fieldDeclaration.getVariables().toString());
            } else if (node instanceof ConstructorDeclaration) {
                ConstructorDeclaration constructorDeclaration = (ConstructorDeclaration) node;
                speech += formatStringWithENDL(constructorDeclaration.getNameAsString());
                speech += formatStringWithENDL(constructorDeclaration.getParameters().toString());
                speech += getStatementListSpeechText(constructorDeclaration.getBody().getStatements());
            } else if (node instanceof MethodDeclaration) {
                MethodDeclaration methodDeclaration = (MethodDeclaration) node;
                speech += formatStringWithENDL(methodDeclaration.getNameAsString());
                speech += formatStringWithENDL(methodDeclaration.getParameters().toString());
                if (methodDeclaration.getBody().isPresent()) {
                    speech += getStatementListSpeechText(methodDeclaration.getBody().get().getStatements());
                }
            } else if (node instanceof EnumDeclaration) {
                EnumDeclaration enumDeclaration = (EnumDeclaration) node;
                speech += getSpeechTextFromNodes(enumDeclaration.getChildNodes());
            } else if (node instanceof EnumConstantDeclaration) {
                EnumConstantDeclaration enumConstantDeclaration = (EnumConstantDeclaration) node;
                speech += formatStringWithENDL(enumConstantDeclaration.toString());
            } else if (node instanceof CallableDeclaration) {
                CallableDeclaration callableDeclaration = (CallableDeclaration) node;
                speech += getSpeechTextFromNodes(callableDeclaration.getChildNodes());
            } else if (node instanceof InitializerDeclaration) {
                InitializerDeclaration initializerDeclaration = (InitializerDeclaration) node;
                speech += formatStringWithENDL(initializerDeclaration.toString());
            } else if (node instanceof TypeDeclaration) {
                TypeDeclaration typeDeclarationMember = (TypeDeclaration) node;
                speech += formatStringWithENDL(typeDeclarationMember.toString());
            } else if (node instanceof ImportDeclaration) {
                ImportDeclaration importDeclaration = (ImportDeclaration) node;
                //speech += formatStringWithENDL(importDeclaration.getNameAsString());
            } else if (node instanceof MarkerAnnotationExpr) {
                MarkerAnnotationExpr markerAnnotationExpr = (MarkerAnnotationExpr) node;
                speech += formatStringWithENDL(markerAnnotationExpr.toString());
            }
        }
        return speech;
    }

    public static String getJavaClassSpeechText(String classPath) throws FileNotFoundException {
        String speech = "";
        CompilationUnit compilationUnit = getCompilationUnitFromClassPath(classPath);
        speech += formatStringWithENDL(compilationUnit.getComments().toString());
        speech += getSpeechTextFromNodes(compilationUnit.getChildNodes());
        return speech;
    }

}
