package parsers;

import fileio.FileHReader;
import fileio.FileHWriter;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import utils.TextUtils;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static utils.TextUtils.formatStringWithENDL;

public class SRCMLHParser {

    public static void parseJavaClassToXML(String javaFilePath, String saveFilePath) throws IOException, InterruptedException {
        Runtime runtime = Runtime.getRuntime();
        Process process = runtime.exec("srcml " + javaFilePath + " > " + saveFilePath + " --position");
        BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
        while ((reader.readLine()) != null) {
        }
        process.waitFor(5, TimeUnit.MINUTES);
        process.destroy();
    }

    public static String generateSRCMLFromJavaCode(String javaCode) throws IOException, InterruptedException {
        String javaCodeTempFile = FileHWriter.generateTempJavaFile(javaCode);

        Runtime runtime = Runtime.getRuntime();
        Process process = runtime.exec("srcml " + javaCodeTempFile + " --position");
        BufferedReader inputReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
        String consoleOutput;
        String srcmlResult = "";
        while ((consoleOutput = inputReader.readLine()) != null) {
            srcmlResult += consoleOutput;
        }
        process.waitFor(5, TimeUnit.MINUTES);
        process.destroy();
        return FileHWriter.generateTempTxtFile(srcmlResult);
    }

    public static List<String> generateClassMethodMapping(String srcmlFilePath) throws ParserConfigurationException, IOException, SAXException {

        File inputFile = new File(srcmlFilePath);
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(inputFile);
        doc.getDocumentElement().normalize();

        return getMethodMap(doc.getDocumentElement());
    }

    public static List<String> getMethodMap(Node parentNode) {
        List<String> methods = new ArrayList<>();

        NodeList childNodes = parentNode.getChildNodes();
        for (int index = 0; index < childNodes.getLength(); index++) {
            Node currentNode = childNodes.item(index);
            if (currentNode.getNodeName().contains("function")) {
                String fuctionHeader = "";
                NodeList functionChildNodes = currentNode.getChildNodes();
                for (int indexFunction = 0; indexFunction < functionChildNodes.getLength(); indexFunction++) {
                    Node functionChildNode = functionChildNodes.item(indexFunction);
                    if (!functionChildNode.getNodeName().contains("block")) {
                        fuctionHeader += functionChildNode.getTextContent();
                    }

                }
                methods.add(fuctionHeader);
            }
            methods.addAll(getMethodMap(currentNode));
        }
        return methods;
    }

    public static String getJavaFileSpeechText(String javaFilePath) throws IOException, SAXException, ParserConfigurationException, InterruptedException {
        String speech = "";
        String javaFileContent = FileHReader.getFileContent(javaFilePath);
        String srcmlFilePath = generateSRCMLFromJavaCode(javaFileContent);
        File inputFile = new File(srcmlFilePath);
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(inputFile);
        doc.getDocumentElement().normalize();

        NodeList nodeList = doc.getChildNodes();

        for (int index = 0; index < nodeList.getLength(); index++) {
            Node node = nodeList.item(index);
            if (!node.getTextContent().isEmpty() && !node.getNodeName().contains("specifier")) {
                speech += formatStringWithENDL(node.getTextContent());
            }
        }
        return speech;
    }

    public static String getLinesOfcode(String srcmlFilePath, List<String> locs) throws ParserConfigurationException, IOException, SAXException {

        File inputFile = new File(srcmlFilePath);
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(inputFile);
        doc.getDocumentElement().normalize();

        return getLinesOfCodeFromNode(doc.getDocumentElement(), locs);
    }

    public static String getLinesOfCodeFromNode(Node parentNode, List<String> locs) {
        String nodeBody = "";
        NodeList nodeList = parentNode.getChildNodes();
        for (int index = 0; index < nodeList.getLength(); index++) {
            Node currentNode = nodeList.item(index);

            if (currentNode.getNodeType() == Node.TEXT_NODE && parentNodeInLOC(currentNode, locs)) {
                nodeBody += currentNode.getTextContent();
                if (currentNode.getParentNode().getNodeName().contains("specifier")) {
                    nodeBody += " ";
                }
            }
            nodeBody += getLinesOfCodeFromNode(currentNode, locs);
        }
        return nodeBody;
    }

    public static boolean parentNodeInLOC(Node currentNode, List<String> locs) {
        Node parentNode = currentNode.getParentNode();
        if (parentNode != null) {
            if (parentNode.getAttributes() != null && parentNode.getAttributes().getNamedItem("pos:line") != null &&
                    locs.indexOf(parentNode.getAttributes().getNamedItem("pos:line").getNodeValue()) > -1) {
                return true;
            } else {
                return parentNodeInLOC(parentNode, locs);
            }
        } else {
            return false;
        }
    }

    public static List<String> getCallGraph(String javaCode) throws IOException, InterruptedException, ParserConfigurationException, SAXException {
        String xmlFilePath = generateSRCMLFromJavaCode(javaCode);
        File inputFile = new File(xmlFilePath);
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(inputFile);
        doc.getDocumentElement().normalize();
        return getCallsFromNode(doc.getDocumentElement());
    }

    public static List<String> getCallsFromNode(Node parentNode) {
        List<String> calls = new ArrayList<>();
        NodeList childNodes = parentNode.getChildNodes();
        for (int index = 0; index < childNodes.getLength(); index++) {
            Node currentNode = childNodes.item(index);
            if (currentNode.getNodeName().contains("call") && parentNode.getNodeName().contains("expr")) {
                List<String> tokens = TextUtils.splitByPoint(currentNode.getTextContent());
                calls.add(tokens.get(tokens.size() - 1));
            } else {
                calls.addAll(getCallsFromNode(currentNode));
            }
        }
        return calls;
    }

    public static List<String> getClassMethodBySignature(String javaFilePath, String methodSignature) throws IOException, SAXException, InterruptedException, ParserConfigurationException {
        String srcmlFilePath = generateSRCMLFromJavaCode(FileHReader.getFileContent(javaFilePath));
        File inputFile = new File(srcmlFilePath);
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(inputFile);
        doc.getDocumentElement().normalize();
        return searchMethodInClass(doc.getDocumentElement(), methodSignature);
    }

    public static List<String> searchMethodInClass(Node parentNode, String methodSignature) {
        List<String> methods = new ArrayList<>();
        NodeList childNodes = parentNode.getChildNodes();
        for (int index = 0; index < childNodes.getLength(); index++) {
            Node currentNode = childNodes.item(index);
            if (currentNode.getNodeName().contains("function")) {
                String functionHeader = "";
                NodeList functionChildNodes = currentNode.getChildNodes();
                for (int indexFunction = 0; indexFunction < functionChildNodes.getLength(); indexFunction++) {
                    Node functionChildNode = functionChildNodes.item(indexFunction);
                    if (!functionChildNode.getNodeName().contains("block")) {
                        functionHeader += functionChildNode.getTextContent();
                    }
                }
                if (functionHeader.contains(methodSignature)) {
                    methods.add(currentNode.getTextContent());
                }
            }
            methods.addAll(searchMethodInClass(currentNode, methodSignature));
        }
        return methods;
    }
}
