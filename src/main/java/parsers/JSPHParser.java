package parsers;

import com.github.javaparser.JavaParser;
import com.github.javaparser.ParseProblemException;
import com.github.javaparser.ast.stmt.BlockStmt;
import fileio.FileHReader;
import fileio.FileHWriter;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class JSPHParser {

    private static String javaCodeStartTag = "<%";
    private static String javaCodeEndTag = "%>";
    private static String replacement = "";


    public static String getJSPSpeechText(String filePath) throws IOException, InterruptedException, SAXException, ParserConfigurationException, JSONException {
        String speech = "";
        String fullText = FileHReader.getFileContent(filePath);
        ArrayList<String> javaScriptFragments = (ArrayList<String>) getJSPJavaScriptFragments(filePath);
        ArrayList<String> javaSnippets = (ArrayList<String>) getJSPJavaSnippets(filePath);

        String javaScriptCode = "";
        for (String javaScriptFragment : javaScriptFragments) {
            javaScriptCode += javaScriptFragment;
        }
        for (String code : javaSnippets) {
            fullText = fullText.replace(code, replacement);
        }
        speech += JavaScriptHParser.getJavaScriptBlockSpeechText(javaScriptCode);
        speech += getSpeechTextFromJavaSnippets(javaSnippets);
        speech += Jsoup.parse(fullText).wholeText().replaceAll("(\\r|\\n)", "");
        return speech;
    }

    public static List<String> getJSPJavaScriptFragments(String filePath) throws IOException {
        List<String> javaScriptCodes = new ArrayList<>();
        File inputFile = new File(filePath);
        Document jspDocument = Jsoup.parse(inputFile, "UTF-8", "");
        Elements scriptsElements = jspDocument.getElementsByTag("script");
        for (Element element : scriptsElements) {
            if (!element.data().trim().isEmpty()) {
                {
                    javaScriptCodes.add(element.data());
                }
            }
        }
        return javaScriptCodes;
    }


    public static List<String> getJSPJavaSnippets(String filePath) throws IOException {
        String jspContent = FileHReader.getFileContent(filePath);
        ArrayList<String> javaSnippets = new ArrayList<>();
        int numberStartTags = StringUtils.countMatches(jspContent, javaCodeStartTag);
        int numberEndTags = StringUtils.countMatches(jspContent, javaCodeEndTag);
        while (jspContent.contains(javaCodeStartTag) && jspContent.contains(javaCodeEndTag)
                && numberStartTags == numberEndTags) {
            int startIndex = jspContent.indexOf(javaCodeStartTag);
            int endIndex = jspContent.indexOf(javaCodeEndTag);
            String serverCode = "";
            try {
                serverCode = jspContent.substring(startIndex, endIndex + 2);
            } catch (Exception e) {
                System.out.println("jsp java snippets exception:");
                System.out.println("startIndex:{" + startIndex + "}");
                System.out.println("endIndex:{" + endIndex + "}");
                //System.out.println("jspContent:{"+jspContent+"}");
                return javaSnippets;
            }
            javaSnippets.add(serverCode);
            jspContent = jspContent.replace(serverCode, replacement);
        }
        return javaSnippets;
    }


    public static List<BlockStmt> generateJavaBlockStatements(List<String> snippets) {
        List<String> javaFragments = getJavaFragmentsFromSnippets(snippets);
        String codeStash = "";
        ArrayList<BlockStmt> blockStatements = new ArrayList<>();
        for (String fragment : javaFragments) {
            try {
                String blockText = "{" + codeStash + fragment + "}";
                BlockStmt block = JavaParser.parseBlock(blockText);
                blockStatements.add(block);
                codeStash = "";
            } catch (ParseProblemException e) {
                if (e.getProblems().get(0).getMessage().contains("Parse error. Found <EOF>")) {
                    fragment = "\n" + fragment + "\n";
                } else {
                    fragment = "\n/* " + fragment + " */\n";
                }
                codeStash += fragment;
            }
        }
        return blockStatements;
    }

    public static List<String> getJavaFragmentsFromSnippets(List<String> snippets) {
        List<String> javaFragments = new ArrayList<>();
        for (String snippet : snippets) {
            javaFragments.add(snippet.replace(javaCodeStartTag, replacement).replace(javaCodeEndTag, replacement).replace("@", ""));
        }
        return javaFragments;
    }

    public static String getFullJavaCodeFromBlockStatements(List<BlockStmt> blockStmts) {
        String javaCode = "";
        for (BlockStmt blockStmt : blockStmts) {
            javaCode += blockStmt.toString().substring(1, blockStmt.toString().length() - 1);
        }
        return javaCode;
    }

    public static String getSpeechTextFromJavaSnippets(List<String> snippets) throws IOException, InterruptedException, ParserConfigurationException, SAXException {
        String speech = "";
        ArrayList<BlockStmt> blockStatements = (ArrayList<BlockStmt>) generateJavaBlockStatements(snippets);
        speech += SRCMLHParser.getJavaFileSpeechText(FileHWriter.generateTempJavaFile(getFullJavaCodeFromBlockStatements(blockStatements)));
        return speech;
    }


}
