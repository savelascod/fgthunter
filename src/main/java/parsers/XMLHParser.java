package parsers;

import fileio.FileHReader;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.util.List;

import static utils.TextUtils.formatStringWithENDL;

public class XMLHParser {

    public static String getXMLSpeechText(String xmlFileContent, boolean includeTags, boolean includeTagsAttributes) throws ParserConfigurationException {
        String speech = "";

        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();

        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc;
        try {
            doc = dBuilder.parse(new InputSource(new StringReader(xmlFileContent)));
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
        doc.getDocumentElement().normalize();
        NodeList nodeList = doc.getElementsByTagName("*");

        for (int index = 0; index < nodeList.getLength(); index++) {
            Node node = nodeList.item(index);
            if (includeTags) {
                speech += formatStringWithENDL(node.getNodeName());
            }
            if (includeTagsAttributes) {
                String attributes = "";
                for (int attrIndex = 0; attrIndex < node.getAttributes().getLength(); attrIndex++) {
                    attributes += "{" + node.getAttributes().item(attrIndex).toString() + "}";
                }
                speech += formatStringWithENDL("[" + attributes + "]");
            }
            if (!node.getTextContent().isEmpty()) {
                speech += formatStringWithENDL(node.getTextContent());
            }
        }
        return speech;
    }

    public static String getXMLSpeechTextFromFile
            (String xmlFilePath, boolean includeTags, boolean includeTagsAttributes) throws IOException, ParserConfigurationException {
        return getXMLSpeechText(FileHReader.getFileContent(xmlFilePath), includeTags, includeTagsAttributes);
    }

    public static String searchTagsByCriteria(String xmlFilePath, List<String> criteria) throws ParserConfigurationException {
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc;
        try {
            doc = dBuilder.parse(new File(xmlFilePath));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        doc.getDocumentElement().normalize();

        return getTagsByCriteria(doc, doc.getDocumentElement(), criteria);
    }

    public static String getTagsByCriteria(Document doc, Node parentNode, List<String> criteria) {
        String nodeBody = "";
        Node currentNode = parentNode.getFirstChild();

        while (currentNode != null) {
            boolean accepted = false;
            String elementInnerXml = innerXml(currentNode);
            for (String criterion : criteria) {
                if (currentNode.getNodeName().toLowerCase().contains(criterion.toLowerCase()) ||
                        (currentNode.getTextContent() != null && (currentNode.getTextContent().toLowerCase().contains(criterion.toLowerCase())))) {
                    accepted = true;
                    break;
                }
                if (currentNode.getAttributes() != null) {
                    int numAttrs = currentNode.getAttributes().getLength();
                    for (int i = 0; i < numAttrs; i++) {
                        Attr attr = (Attr) currentNode.getAttributes().item(i);
                        if (attr.getNodeName().toLowerCase().contains(criterion.toLowerCase()) || attr.getNodeValue().toLowerCase().contains(criterion.toLowerCase())) {
                            accepted = true;
                            break;
                        }
                    }
                }
            }
            if (accepted) {
                nodeBody += elementInnerXml;
            } else {
                nodeBody += getTagsByCriteria(doc, currentNode, criteria);
            }

            currentNode = currentNode.getNextSibling();
        }

        return nodeBody;
    }

    private static String innerXml(Node node) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            Source source = new DOMSource(node);
            Result target = new StreamResult(out);
            transformer.transform(source, target);
        } catch (TransformerException e) {
            e.printStackTrace();
        }
        return out.toString();
    }
}
