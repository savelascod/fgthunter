package parsers;

import fileio.FileHReader;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static utils.TextUtils.formatStringWithENDL;

public class FileHParser {

    public static String getFileSpeechText(String filePath) throws IOException {
        String speech = formatStringWithENDL(FileHReader.getFileContent(filePath));
        return speech;
    }

    public static List<String> getFileLinesByCriteria(String filePath, List<String> criteria) {
        List<String> filteredFileLines = new ArrayList<>();

        try {
            for (String fileLine : FileHReader.getFileLines(filePath)) {
                for (String criterion : criteria) {
                    if (fileLine.toLowerCase().contains(criterion.toLowerCase())) {
                        filteredFileLines.add(fileLine);
                        break;
                    }
                }
            }
        } catch (IOException e) {
        }
        return filteredFileLines;
    }

}
