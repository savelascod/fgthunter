package parsers;

import fileio.FileHReader;
import jdk.nashorn.internal.ir.*;
import jdk.nashorn.internal.ir.visitor.NodeVisitor;
import jdk.nashorn.internal.parser.Parser;
import jdk.nashorn.internal.runtime.Context;
import jdk.nashorn.internal.runtime.ErrorManager;
import jdk.nashorn.internal.runtime.Source;
import jdk.nashorn.internal.runtime.options.Options;
import org.json.JSONException;
import utils.TextUtils;

import java.io.IOException;
import java.util.List;

/**
 * Created by mordreth on 4/11/18.
 */
public class JavaScriptHParser {

    public static String getJavaScriptFileSpeechText(String jsFilePath) throws IOException, JSONException {
        String fileContent = FileHReader.getFileContent(jsFilePath);
        return getJavaScriptBlockSpeechText(fileContent);
    }


    public static String getJavaScriptBlockSpeechText(String jsBlock) throws JSONException {
        final String[] speech = {""};

        /*
        // esto obtiene un AST formato JSON del javascript
        Options options = new Options("nashorn");
        options.set("anon.functions", true);
        options.set("parse.only", true);
        options.set("scripting", true);

        ErrorManager errors = new ErrorManager();
        Context context = new Context(options, errors, Thread.currentThread().getContextClassLoader());
        Context.setGlobal(context.createGlobal());

        String parseTree = ScriptUtils.parse(jsBlock, "nashorn", false).replaceAll("\"\"", "\" \"");
        JSONObject jsonTree = new JSONObject(parseTree);
        //System.out.println("<-->"+jsonTree+"<-->");
        String xml = ("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" +
                XML.toString(jsonTree, "root")).replaceAll("[^\\x20-\\x7e\\x0A]", "");
        return XMLHParser.getXMLSpeechText(xml, false, false);
        */


        Options options = new Options("nashorn");
        options.set("anon.functions", true);
        options.set("parse.only", true);
        options.set("scripting", true);

        ErrorManager errors = new ErrorManager();
        Context context = new Context(options, errors, Thread.currentThread().getContextClassLoader());
        Source source = Source.sourceFor("test", jsBlock);
        Parser parser = new Parser(context.getEnv(), source, errors);
        Block block = null;

        try {
            FunctionNode functionNode = parser.parse();
            block = functionNode.getBody();
        } catch (Exception e) {
            return "";
        }

        List<Statement> statements = block.getStatements();

        for (Statement jsStatement : statements) {
            jsStatement.accept(new NodeVisitor<LexicalContext>(new LexicalContext()) {
                @Override
                public boolean enterExpressionStatement(ExpressionStatement expressionStatement) {
                    if (expressionStatement.getExpression() != null) {
                        //speech[0] += formatStringWithENDL(expressionStatement.getExpression().toString(false));
                    }
                    return super.enterExpressionStatement(expressionStatement);
                }

                @Override
                public boolean enterLiteralNode(LiteralNode<?> literalNode) {
                    speech[0] += TextUtils.formatStringWithENDL(literalNode.toString(false));
                    return super.enterLiteralNode(literalNode);
                }

                @Override
                public boolean enterVarNode(VarNode varNode) {

                    if (!varNode.isFunctionDeclaration()) {
                        speech[0] += TextUtils.formatStringWithENDL(varNode.getName().toString(false));
                    }

                    return super.enterVarNode(varNode);
                }

                //control structures condition
                @Override
                public boolean enterIfNode(IfNode ifNode) {
                    if (ifNode.getTest() != null) {
                        speech[0] += TextUtils.formatStringWithENDL(ifNode.getTest().toString(false));
                    }
                    return super.enterIfNode(ifNode);
                }
                @Override
                public boolean enterSwitchNode(SwitchNode switchNode) {
                    if (switchNode.getExpression() != null) {
                        speech[0] += TextUtils.formatStringWithENDL(switchNode.getExpression().toString(false));
                    }
                    return super.enterSwitchNode(switchNode);
                }

                @Override
                public boolean enterCaseNode(CaseNode caseNode) {
                    if (caseNode.getEntry() != null) {
                        speech[0] += TextUtils.formatStringWithENDL(caseNode.getEntry().toString());
                    }
                    return super.enterCaseNode(caseNode);
                }
                @Override
                public boolean enterWhileNode(WhileNode whileNode) {
                    if (whileNode.getTest() != null) {
                        speech[0] += TextUtils.formatStringWithENDL(whileNode.getTest().toString(false));
                    }
                    return super.enterWhileNode(whileNode);
                }
                @Override
                public boolean enterForNode(ForNode forNode) {
                    if (forNode.getTest() != null) {
                        speech[0] += TextUtils.formatStringWithENDL(forNode.getTest().toString(false));
                    }
                    return super.enterForNode(forNode);
                }
                @Override
                public boolean enterCallNode(CallNode callNode) {
                    speech[0] += TextUtils.formatStringWithENDL(callNode.toString(false));
                    return super.enterCallNode(callNode);
                }
                @Override
                public boolean enterReturnNode(ReturnNode returnNode) {
                    if (returnNode.getExpression() != null) {
                        speech[0] += TextUtils.formatStringWithENDL(returnNode.getExpression().toString(false));
                    }
                    return super.enterReturnNode(returnNode);
                }

                @Override
                public boolean enterCatchNode(CatchNode catchNode) {
                    if (catchNode.getException() != null) {
                        speech[0] += TextUtils.formatStringWithENDL(catchNode.getException().toString(false));
                    }
                    return super.enterCatchNode(catchNode);
                }
            });
        }

        return speech[0];


    }


}
