package parsers;


import fileio.FileHReader;
import net.sf.jsqlparser.parser.CCJSqlParserManager;
import net.sf.jsqlparser.statement.Statement;
import net.sf.jsqlparser.statement.alter.Alter;
import net.sf.jsqlparser.statement.create.table.ColumnDefinition;
import net.sf.jsqlparser.statement.create.table.CreateTable;
import net.sf.jsqlparser.statement.insert.Insert;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import static utils.TextUtils.formatStringWithENDL;

public class SQLHParser {

    public static String getSQLStatementSpeechText(String sqlStatement) {
        String speech = "";
        CCJSqlParserManager pm = new CCJSqlParserManager();
        sqlStatement = sqlStatement.replaceAll("UNIQUE", "").replaceAll("(\\r|\\n)", "");
        Statement statement = null;
        if (sqlStatement.isEmpty() || sqlStatement == null) {
            return null;
        }
        try {
            statement = pm.parse(new StringReader(sqlStatement));
        } catch (Exception e) {
            //System.out.println("conflict statement: {" + sqlStatement + "}");
            speech += formatStringWithENDL(sqlStatement);
        }
        if (statement instanceof CreateTable) {
            CreateTable createTableStm = (CreateTable) statement;
            speech += formatStringWithENDL(createTableStm.getTable().getFullyQualifiedName());
            for (ColumnDefinition columnDefinition : createTableStm.getColumnDefinitions()) {
                speech += formatStringWithENDL(columnDefinition.getColumnName());
            }
        } else if (statement instanceof Insert) {
            Insert insertStm = (Insert) statement;
            try {
                speech += formatStringWithENDL(insertStm.getColumns().toString() + insertStm.getItemsList().toString());
            } catch (Exception e) {
                //System.out.println("Exception sql file");
            }
        } else if (statement instanceof Alter) {
            Alter alterStm = (Alter) statement;
            speech += formatStringWithENDL(alterStm.toString());
        }
        return speech;
    }

    public static String getSQLFileSpeechText(String sqlFilePath) throws IOException {
        String speech = "";

        String fileContent = FileHReader.getFileContent(sqlFilePath);
        String[] sqlStatements = fileContent.split(";");

        for (String sqlStm : sqlStatements) {
            //System.out.print(".");
            speech += getSQLStatementSpeechText(sqlStm);
        }
        return speech;
    }

    public static List<String> searchSQLStamentByCriteria(String sqlFilePath, List<String> criteria) throws IOException {
        String fileContent = FileHReader.getFileContent(sqlFilePath);
        String[] sqlStatements = fileContent.split(";");
        ArrayList<String> sqlFoundStatements = new ArrayList<>();

        for (String sqlStatement : sqlStatements) {
            CCJSqlParserManager pm = new CCJSqlParserManager();
            sqlStatement = sqlStatement.replaceAll("UNIQUE", "").replaceAll("(\\r|\\n)", "");
            Statement statement = null;
            if (!(sqlStatement.isEmpty() || sqlStatement == null)) {
                try {
                    statement = pm.parse(new StringReader(sqlStatement));
                } catch (Exception e) {
                    //System.out.println("conflict statement: {" + sqlStatement + "}");
                    //System.out.println("conflict statement: {" + sqlStatement + "}");
                }

                for (String criterion : criteria) {
                    if (sqlStatement.contains(criterion) || sqlStatement.contains(criterion.toUpperCase()) || sqlStatement.contains(criterion.toLowerCase())) {
                        sqlFoundStatements.add(sqlStatement);
                    }
                }
            }

        }
        return sqlFoundStatements;
    }


}
