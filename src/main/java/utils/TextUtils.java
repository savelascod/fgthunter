package utils;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by mordreth on 4/11/18.
 */
public class TextUtils {
    public static String formatStringWithENDL(String text) {
        //return "{" + text.replaceAll("(\\r|\\n)", "") + "}" + "\n";
        return text.replaceAll("(\\r|\\n)", "") + "\n";
    }

    public static List<String> getListOfMatches(String text, String regex) {
        List<String> allMatches = new ArrayList<>();
        Matcher m = Pattern.compile(regex).matcher(text);
        while (m.find()) {
            allMatches.add(m.group());
        }
        return allMatches;
    }

    public static List<String> splitByCommas(String content) {
        List<String> splitTokens = new ArrayList<>();
        String token = "";

        if (content.length() <= 0) {
            return splitTokens;
        }

        for (int charIndex = 0; charIndex < content.length(); charIndex++) {
            char character = content.charAt(charIndex);
            switch (character) {
                case ((char) 44):
                    int openCurvyBracketCount = StringUtils.countMatches(token, String.valueOf((char) 123));
                    int closeCurvyBracketCount = StringUtils.countMatches(token, String.valueOf((char) 125));

                    int openParenthesisBracketCount = StringUtils.countMatches(token, String.valueOf((char) 123));
                    int closeParenthesisBracketCount = StringUtils.countMatches(token, String.valueOf((char) 125));

                    if (openCurvyBracketCount != closeCurvyBracketCount || openParenthesisBracketCount != closeParenthesisBracketCount) {
                        token += character;
                    } else {
                        splitTokens.add(token);
                        token = "";
                    }
                    break;
                default:
                    token += character;
                    break;
            }
        }
        splitTokens.add(token);
        return splitTokens;
    }

    public static String getContentOfCurvyBrackets(String text) {
        String aux = "";
        String content = "";
        for (int charIndex = 0; charIndex < text.length(); charIndex++) {
            char character = text.charAt(charIndex);
            aux += character;
            int openBracketCount = StringUtils.countMatches(aux, String.valueOf((char) 123));
            int closeBracketCount = StringUtils.countMatches(aux, String.valueOf((char) 125));
            if (aux.contains(String.valueOf((char) 123)) && openBracketCount != closeBracketCount) {
                content += character;
            }
        }

        if (content.length() > 0) {
            return content.substring(1, content.length());
        }
        return null;
    }

    public static String getContentOfParenthesis(String text) {
        String aux = "";
        String content = "";
        for (int charIndex = 0; charIndex < text.length(); charIndex++) {
            char character = text.charAt(charIndex);
            aux += character;
            int openBracketCount = StringUtils.countMatches(aux, String.valueOf((char) 40));
            int closeBracketCount = StringUtils.countMatches(aux, String.valueOf((char) 41));
            if (aux.contains(String.valueOf((char) 123)) && openBracketCount != closeBracketCount) {
                content += character;
            }
        }
        if (content.length() > 0) {
            return content.substring(1, content.length());
        }
        return "";
    }

    public static List<String> splitByPoint(String text) {
        List<String> splitTokens = new ArrayList<>();
        String token = "";
        for (int charIndex = 0; charIndex < text.length(); charIndex++) {
            char character = text.charAt(charIndex);
            int openBracketCount = StringUtils.countMatches(token, String.valueOf((char) 40));
            int closeBracketCount = StringUtils.countMatches(token, String.valueOf((char) 41));
            if (character == (char) 46 && openBracketCount == closeBracketCount) {
                splitTokens.add(token);
                token = "";
            } else {
                token += character;
            }
        }
        splitTokens.add(token);
        return splitTokens;
    }

    public static String getTextWithoutArgs(String text) {
        String content = "";
        for (int charIndex = 0; charIndex < text.length(); charIndex++) {
            char character = text.charAt(charIndex);
            if (character == (char) 40) {
                break;
            }
            content += character;
        }
        return content;
    }

    public static List<String> splitBySeparator(String text, String separator) {
        List<String> splitTokens = new ArrayList<>();
        if (text.indexOf(separator) > -1) {
            splitTokens.add(text.substring(0, text.indexOf(separator)));
            splitTokens.addAll(splitBySeparator(text.substring(text.indexOf(separator) + separator.length(), text.length()), separator));
        } else {
            splitTokens.add(text);
        }
        return splitTokens;
    }

    public static List<String> splitBySpace(String text) {
        List<String> splitTokens = new ArrayList<>();
        String token = "";
        for (int charIndex = 0; charIndex < text.length(); charIndex++) {
            char character = text.charAt(charIndex);
            int openBracketCount = StringUtils.countMatches(token, String.valueOf((char) 40));
            int closeBracketCount = StringUtils.countMatches(token, String.valueOf((char) 41));
            if (character == (char) 32 && openBracketCount == closeBracketCount) {
                splitTokens.add(token);
                token = "";
            } else {
                token += character;
            }
        }
        splitTokens.add(token);
        return splitTokens;
    }

    public static List<String> getTokensWithParenthesis(List<String> tokens) {
        List<String> validTokens = new ArrayList<>();
        for (String token : tokens) {
            int openBracketCount = StringUtils.countMatches(token, String.valueOf((char) 40));
            int closeBracketCount = StringUtils.countMatches(token, String.valueOf((char) 41));

            if (openBracketCount > 0 && closeBracketCount > 0 && openBracketCount == closeBracketCount) {
                validTokens.add(token);
            }
        }
        return validTokens;
    }

    public static String splitByCharacterTypeCamelCase(String word) {
        String[] ccTokens = StringUtils.splitByCharacterTypeCamelCase(word);
        String tokenCC = StringUtils.join(ccTokens, ' ');
        return tokenCC;
    }


}
