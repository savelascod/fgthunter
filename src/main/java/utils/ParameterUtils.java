package utils;

public class ParameterUtils {
    public static final int queryResultsLimit = 25;
    public static String if4se_corpusFileExt = "_Corpus.txt";
    public static String if4se_queryFileExt = "_Queries.txt";
    public static String ir4se_resultsFileExt = "-results.csv";
    public static String tempFolderPath = "/Users/mordreth/Downloads/Temp";
    public static String methodMappingFileExt = "_MethodMappings.txt";
}
