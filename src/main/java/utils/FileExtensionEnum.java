package utils;

/**
 * Created by mordreth on 8/21/18.
 */
public enum FileExtensionEnum {

    JAVA(0, ".java", "JAVA FILE", "JAVA"),
    JSP(1, ".jsp", "JSP FILE", "JSP"),
    JAVASCRIPT(2, ".js", "JAVASCRIPT FILE", "JS"),
    SQL(3, ".sql", "SQL FILE", "SQL"),
    XML(4, ".xml", "XML FILE", "XML"),
    PROPERTIES(5, ".properties", "PROPERTIES FILE", "PROPS");
    private Integer id;
    private String extension;
    private String description;
    private String abbreviation;


    FileExtensionEnum(Integer id, String extension, String description, String abbreviation) {
        this.id = id;
        this.extension = extension;
        this.description = description;
        this.abbreviation = abbreviation;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }
}
