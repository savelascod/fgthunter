package fileio;

import net.quux00.simplecsv.CsvParser;
import net.quux00.simplecsv.CsvParserBuilder;
import net.quux00.simplecsv.CsvReader;
import net.quux00.simplecsv.CsvReaderBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import utils.ParameterUtils;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FileHReader {


    public static Map<String, String> readParamsFile(String filePath) throws ParserConfigurationException {
        Map<String, String> params = new HashMap<>();

        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc;
        try {
            doc = dBuilder.parse(new File(filePath));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        doc.normalizeDocument();
        NodeList nodeList = doc.getDocumentElement().getElementsByTagName("param");
        for (int index = 0; index < nodeList.getLength(); index++) {
            Node currentParamNode = nodeList.item(index);

            if (currentParamNode.getAttributes().getNamedItem("name").getNodeValue().equals("folder_path")) {
                params.put("folder_path", currentParamNode.getTextContent());
            }
            if (currentParamNode.getAttributes().getNamedItem("name").getNodeValue().equals("project_id")) {
                params.put("project_id", currentParamNode.getTextContent());
            }
            if (currentParamNode.getAttributes().getNamedItem("name").getNodeValue().equals("project_dir_path")) {
                params.put("project_dir_path", currentParamNode.getTextContent());
            }
            if (currentParamNode.getAttributes().getNamedItem("name").getNodeValue().equals("prequery_file_path")) {
                params.put("prequery_file_path", currentParamNode.getTextContent());
            }
        }
        params.put("ret_model", "LUCENE");
        params.put("system", params.get("project_id"));
        params.put("base_dir", params.get("folder_path"));
        params.put("queries_dir", params.get("folder_path"));
        params.put("name_config", params.get("project_id"));
        params.put("folder_path", params.get("folder_path") + System.getProperty("file.separator") + params.get("project_id"));


        return params;
    }


    public static String getFileContent(String filepath) throws IOException {
        byte[] encoded = Files.readAllBytes(Paths.get(filepath));
        return new String(encoded);
    }

    public static List<String> getFileLines(String filePath) throws IOException {
        List<String> fileLines = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(filePath))) {
            String line;
            while ((line = br.readLine()) != null) {
                fileLines.add(line);
            }
        }
        return fileLines;
    }


    public static Map<Integer, List<String>> readPreQueryFile(String filePath) throws IOException {
        Map<Integer, List<String>> entries = new HashMap<>();
        System.out.println(filePath);
        FileReader fr = new FileReader(filePath);
        CsvReader csvr = new CsvReader(fr);
        List<String> tokens;
        int totalEntries = 1;
        while ((tokens = csvr.readNext()) != null) {
            entries.put(totalEntries, tokens);
            totalEntries += 1;
        }
        return entries;
    }


    public static List<String> readStopWordsFromFile(String filePath) throws IOException {
        List<String> stopWords = new ArrayList<>();
        File file = new File(filePath);
        BufferedReader br = new BufferedReader(new FileReader(file));
            String line;
            while ((line = br.readLine()) != null) {
                stopWords.add(line);
            }

        return stopWords;
    }

    public static Map<Integer, Object> readIR4SEResults(String resultFilePath) throws IOException {
        Map<Integer, Object> results = new HashMap<>();
        FileReader fr = new FileReader(resultFilePath);
        CsvParser p = new CsvParserBuilder().
                separator(';').
                build();
        CsvReader csvr = new CsvReaderBuilder(fr).csvParser(p).build();

        List<String> row;
        int register = 0;
        while ((row = csvr.readNext()) != null) {
            if (register != 0) {
                Integer queryId = Integer.valueOf(row.get(0));
                List<String> queryResult = new ArrayList<>();
                queryResult.add(row.get(2));
                queryResult.add(row.get(4));
                queryResult.add(row.get(5));
                if (results.get(queryId) != null &&
                        ((ArrayList<Object>) results.get(queryId)).size() < ParameterUtils.queryResultsLimit) {
                    ((ArrayList<Object>) results.get(queryId)).add(queryResult);
                } else if (results.get(queryId) == null) {
                    List<Object> queryResults = new ArrayList<>();
                    queryResults.add(queryResult);
                    results.put(queryId, queryResults);
                }
            }

            register += 1;
        }
        return results;
    }

}
