package fileio;

import utils.FileExtensionEnum;
import utils.ParameterUtils;
import utils.TextUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FileHExplorer {

    public static List<File> getFilesFromProjectByExtension(String directoryPath, String extension) {
        List fileList = new ArrayList();
        try {
            File directory = new File(directoryPath);
            for (File file : directory.listFiles()) {
                if (file.isFile() && file.getName().endsWith(extension)) {
                    fileList.add(file);
                } else if (file.isDirectory()) {
                    fileList.addAll(getFilesFromProjectByExtension(file.getAbsolutePath(), extension));
                }
            }
        } catch (Exception e) {
        }
        return fileList;
    }

    public static List<File> getJavaFilesFromProject(String projectDirPath) {
        return getFilesFromProjectByExtension(projectDirPath, ".java");
    }

    public static List<File> getJavaScriptFilesFromProject(String projectDirPath) {
        return getFilesFromProjectByExtension(projectDirPath, ".js");
    }

    public static List<File> getJSPFilesFromProject(String projectDirPath) {
        return getFilesFromProjectByExtension(projectDirPath, ".jsp");
    }

    public static List<File> getSQLFilesFromProject(String projectDirPath) {
        return getFilesFromProjectByExtension(projectDirPath, ".sql");
    }

    public static List<File> getXMLFilesFromProject(String projectDirPath) {
        return getFilesFromProjectByExtension(projectDirPath, ".xml");
    }

    public static List<File> getPropsFilesFromProject(String projectDirPath) {
        return getFilesFromProjectByExtension(projectDirPath, FileExtensionEnum.PROPERTIES.getExtension());
    }


    public static List<String> searchClassesContainingMethodName(String folderPath, String projectId, String method) throws IOException {
        File file = new File(folderPath + System.getProperty("file.separator") + projectId + ParameterUtils.methodMappingFileExt);
        List<String> classes = new ArrayList<>();
        BufferedReader br = new BufferedReader(new FileReader(file));
        String line;
        while ((line = br.readLine()) != null) {
            List<String> entry = TextUtils.splitBySeparator(line, "<sp>");
            if (entry.size() == 2) {
                List<String> entryMethods = TextUtils.splitByCommas(entry.get(1).substring(2, entry.get(1).length() - 2));
                for (String entryMethod : entryMethods) {
                    List<String> functionTokens = TextUtils.getTokensWithParenthesis(TextUtils.splitBySpace(entryMethod.replace(String.valueOf((char) 9), " ")));
                    String functionSignature = null;
                    if (functionTokens.size() > 1) {
                        for (String functionToken : functionTokens) {
                            if (!functionToken.startsWith("@")) {
                                functionSignature = functionToken;
                            }
                        }
                    } else if (functionTokens.size() == 1) {
                        functionSignature = functionTokens.get(0);
                    }
                    if (functionSignature != null && !functionSignature.trim().isEmpty()) {
                        int totalSignatureParams = TextUtils.splitByCommas(TextUtils.getContentOfParenthesis(functionSignature)).size();
                        int totalMethodParams = TextUtils.splitByCommas(TextUtils.getContentOfParenthesis(method)).size();
                        if (totalSignatureParams == totalMethodParams && TextUtils.getTextWithoutArgs(method).equals(TextUtils.getTextWithoutArgs(functionSignature))) {
                            classes.add(entry.get(0));
                        }
                    }
                }

            }
        }
        return classes;
    }

}
