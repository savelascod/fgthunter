package fileio;

import utils.ParameterUtils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

public class FileHWriter {

    public static String generateTempJavaFile(String content) throws IOException {
        return createFile(ParameterUtils.tempFolderPath, "temp.java", content);
    }

    public static String generateTempTxtFile(String content) throws IOException {
        return createFile(ParameterUtils.tempFolderPath, "temp.txt", content);
    }

    public static String createFile(String folderPath, String fileName, String content) throws IOException {
        Path filePath = Paths.get(folderPath + System.getProperty("file.separator") + fileName);
        Files.deleteIfExists(filePath);
        Files.createDirectories(filePath.getParent());
        Files.createFile(filePath);
        Files.write(filePath, content.getBytes(), StandardOpenOption.WRITE);
        return filePath.toString();
    }

    public static void generateMethodMappingFile(String folderPath, String projectId) throws IOException {
        createFile(folderPath, projectId + ParameterUtils.methodMappingFileExt, "");
    }

    public static void generateIRF4SECorpusFile(String folderPath, String projectId) throws IOException {
        createFile(folderPath, projectId + ParameterUtils.if4se_corpusFileExt, "");
    }

    public static void generateIRF4SEQueryFile(String folderPath, String projectId) throws IOException {
        createFile(folderPath, projectId + ParameterUtils.if4se_queryFileExt, "");
    }

    public static void addIRF4SECorpusFileEntry(String folderPath, String projectId, String entryId, String entryContent) throws IOException {
        String content = "\"" + entryId + "\"" + ";" + "\"" + entryContent + "\"" + System.getProperty("line.separator");
        Files.write(Paths.get(folderPath + System.getProperty("file.separator") + projectId + ParameterUtils.if4se_corpusFileExt), content.getBytes(), StandardOpenOption.APPEND);
    }

    public static void addIRF4SEQueryFileEntry(String folderPath, String projectId, Integer entryIndex, String entryId, String entryContent) throws IOException {
        String content = entryIndex + " " + projectId + "-" + entryId + System.getProperty("line.separator");
        content += entryContent + System.getProperty("line.separator");
        content += "0" + System.getProperty("line.separator") + System.getProperty("line.separator");
        Files.write(Paths.get(folderPath + System.getProperty("file.separator") + projectId + ParameterUtils.if4se_queryFileExt), content.getBytes(), StandardOpenOption.APPEND);
    }

    public static void addIRF4SECorpusFileEntry(String folderPath, String projectId, String entryContent) throws IOException {
        String content = "\"" + entryContent + "\"" + System.getProperty("line.separator");
        Files.write(Paths.get(folderPath + System.getProperty("file.separator") + projectId + ParameterUtils.if4se_corpusFileExt), content.getBytes(), StandardOpenOption.APPEND);
    }

    public static void addMethodMappingFileEntry(String folderPath, String projectId, String entryId, String entryContent) throws IOException {
        String content = "\"" + entryId + "\"" + "<sp>" + "\"" + entryContent + "\"" + System.getProperty("line.separator");
        Files.write(Paths.get(folderPath + System.getProperty("file.separator") + projectId + ParameterUtils.methodMappingFileExt), content.getBytes(), StandardOpenOption.APPEND);
    }

    public static void writeFGTHunterResultsFile(String folderPath, String projectId, String fileContent, String fileName, String fileExt, Integer queryId) throws IOException {
        Files.write(Paths.get(folderPath + System.getProperty("file.separator") + projectId + "_Results" + System.getProperty("file.separator") +
                "RQ_" + queryId + System.getProperty("file.separator") +
                fileExt + System.getProperty("file.separator") + fileName), fileContent.getBytes(), StandardOpenOption.APPEND);
    }

    public static String generateFGTHunterResultsFile(String folderPath, String projectId, String fileContent, String fileName, String fileExt, Integer queryId) throws IOException {
        Path filePath = Paths.get(folderPath + System.getProperty("file.separator") + projectId + "_Results" + System.getProperty("file.separator") +
                "RQ_" + queryId + System.getProperty("file.separator") +
                fileExt + System.getProperty("file.separator") + fileName);
        Files.deleteIfExists(filePath);
        Files.createDirectories(filePath.getParent());
        Files.createFile(filePath);
        Files.write(filePath, fileContent.getBytes(), StandardOpenOption.WRITE);
        return filePath.toString();
    }



}
