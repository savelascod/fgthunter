package irfwk;

import edu.wayne.cs.severe.ir4se.processor.controllers.*;
import edu.wayne.cs.severe.ir4se.processor.controllers.factory.RetrievalFactory;
import edu.wayne.cs.severe.ir4se.processor.controllers.impl.DefaultRetrievalEvaluator;
import edu.wayne.cs.severe.ir4se.processor.controllers.impl.DefaultRetrievalParser;
import edu.wayne.cs.severe.ir4se.processor.controllers.impl.DefaultRetrievalProcessor;
import edu.wayne.cs.severe.ir4se.processor.controllers.impl.DefaultRetrievalWriter;
import edu.wayne.cs.severe.ir4se.processor.entity.*;
import edu.wayne.cs.severe.ir4se.processor.utils.FileRetrievalUtils;
import edu.wayne.cs.severe.ir4se.processor.utils.ParameterUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by mordreth on 5/8/18.
 */
public class RetrievalHProcessor extends RetrievalProcessor {

    private static Logger LOGGER = LoggerFactory.getLogger(DefaultRetrievalProcessor.class);

    public void processSystem(Map<String, String> params) throws Exception {
        String resultFileName = ParameterUtils.getConfigName(params);

        LOGGER.info("Running configuration: " + resultFileName);

        // read the corpus
        RetrievalParser parser = new DefaultRetrievalParser();
        LOGGER.info("Reading the corpus...");
        List<RetrievalDoc> corpusDocs = parser.readCorpus(ParameterUtils.getCorpFilePath(params));

        // get the retrieval from the configuration file
        String retModel = ParameterUtils.getRetrievalModel(params);

        // index the corpus
        RetrievalIndexer indexer = RetrievalFactory.createIndexer(retModel);
        String indxFolder = ParameterUtils.getIndexFolderPath(params);
        FileRetrievalUtils.createEmptyFolder(indxFolder);
        LOGGER.info("Indexing...");
        indexer.buildIndex(indxFolder, corpusDocs, params);

        // create the topic distributions
        TopicDistribution topicDistr;
        RetrievalTopicModeler topicModeler = null;
        try {
            topicModeler = RetrievalFactory.getTopicModeler(indxFolder, retModel);
            topicDistr = null;
            if (topicModeler != null) {
                LOGGER.info("Creating the topic distributions...");
                topicDistr = topicModeler.createTopicDistr(corpusDocs, params);
            }
        } finally {
            if (topicModeler != null) {
                topicModeler.closeIndex();
            }
        }

        // read the queries
        LOGGER.info("Reading the queries...");
        List<Query> queries = parser.readQueries(ParameterUtils.getQueriesFilePath(params),
                ParameterUtils.getQueriesExcludedFilePath(params));

        // initialize the searcher and evaluator
        RetrievalSearcher searcher = RetrievalFactory.getSearcher(retModel, params, indxFolder, topicDistr);
        RetrievalEvaluator evaluator = new DefaultRetrievalEvaluator(corpusDocs.size());

        // initialize the relevance judgments
        LOGGER.info("Reading the relevance judgments...");
        Map<Query, RelJudgment> relJudments = parser.readRelevantJudgments(ParameterUtils.getRelJudFilePath(params),
                corpusDocs);

        // create the query evaluation results
        Map<Query, List<Double>> queryEvals = new LinkedHashMap<Query, List<Double>>();

        Map<Query, List<RetrievalDoc>> queryRes = new HashMap<>();

        // search for every query
        LOGGER.info("Searching...");
        try {
            for (Query query : queries) {

                // get the results
                List<RetrievalDoc> results = null;
                try {
                    results = searcher.searchQuery(query);
                    setNamesResults(results, corpusDocs);
                } catch (Exception e) {
                    LOGGER.error(e.getMessage(), e);
                }

                queryRes.put(query, results);

                // evaluate the relevance judgments
                RelJudgment relJudgment = relJudments.get(query);

                if (relJudgment.getRelevantDocs().isEmpty()) {
                    LOGGER.info("No rel jud evaluation for query: " + query.getQueryId());
                }
                List<Double> resultsRelJud = evaluator.evaluateRelJudgment(relJudgment, results);

                // store the results of the evaluation
                queryEvals.put(query, resultsRelJud);
            }
        } finally {
            if (searcher != null) {
                searcher.close();
            }
        }

        // evaluate the retrieval model
        LOGGER.info("Evaluating the model...");
        RetrievalStats modelStats = evaluator.evaluateModel(queryEvals);

        // write the results
        RetrievalWriter writer = new DefaultRetrievalWriter();

        LOGGER.info("Writing the results...");
        FileRetrievalUtils.removeFile(ParameterUtils.getStatsFilePath(params));
        FileRetrievalUtils.removeFile(ParameterUtils.getResultsFilePath(params));
        writer.writeQueryResults(queryRes, ParameterUtils.getResultsFilePath(params));
        writer.writeStats(modelStats, ParameterUtils.getStatsFilePath(params));

        LOGGER.debug("Stats written in " + ParameterUtils.getStatsFilePath(params));

        LOGGER.info("Done!");
    }

    @Override
    public void processSystem(String s) {

    }
}
