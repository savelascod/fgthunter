package hunter;

import fileio.FileHExplorer;
import fileio.FileHReader;
import fileio.FileHWriter;
import irfwk.RetrievalHProcessor;
import nlp.TextHPreprocessor;
import nlp.TextHProcessor;
import org.apache.commons.cli.*;
import org.json.JSONException;
import org.xml.sax.SAXException;
import parsers.*;
import sca.JavaHCallAnalyzer;
import sca.JavaHSlicer;
import utils.FileExtensionEnum;
import utils.ParameterUtils;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainMassive {

    //private static Map<String, String> params = new HashMap<>();
    private static Map<String, String> params;

    public static void main(String[] args) {

        Map<String, String> arguments = readOptions(args);

        try {
            params = FileHReader.readParamsFile(arguments.get("paramsFile"));
            ParameterUtils.tempFolderPath = params.get("folder_path") + System.getProperty("file.separator") + "temp";
            System.out.println(ParameterUtils.tempFolderPath);
            //setParams();

            if (!arguments.get("corpusIndexing").equals("false")) {
                buildProjectCorpusFile();
            }
            if (!arguments.get("queriesIndexing").equals("false")) {
                buildProjectQueryFile();
            }

            RetrievalHProcessor processor = new RetrievalHProcessor();
            processor.processSystem(params);
            Map<Integer, Object> queriesResultsClassG = FileHReader.readIR4SEResults(params.get("base_dir")
                    + System.getProperty("file.separator") + params.get("system") +
                    System.getProperty("file.separator") + params.get("system") + ParameterUtils.ir4se_resultsFileExt);

            System.out.println("----------------------------------------SLICER-------------------------------------------");
            processIR4SEResults(queriesResultsClassG);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            System.out.println("Error while reading params file, please check parameters.");
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private static Map<String, String> readOptions(String[] args) {
        Map<String, String> arguments = new HashMap<>();
        Options options = new Options();

        Option input = new Option("pf", "params", true, "params file path");
        input.setRequired(true);
        options.addOption(input);

        input = new Option("gc", "corpusIndexing", true, "generate Corpus indexing");
        input.setRequired(false);
        options.addOption(input);

        input = new Option("gq", "queriesIndexing", true, "generate Query indexing");
        input.setRequired(false);
        options.addOption(input);

        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine cmd = null;

        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            System.out.println(e.getMessage());
            formatter.printHelp("utility-name", options);
            System.exit(1);
        }
        arguments.put("paramsFile", cmd.getOptionValue("params"));

        if (cmd.getOptionValue("corpusIndexing") == null) {
            arguments.put("corpusIndexing", "true");
        } else {
            arguments.put("corpusIndexing", cmd.getOptionValue("corpusIndexing"));
        }

        if (cmd.getOptionValue("queriesIndexing") == null) {
            arguments.put("queriesIndexing", "true");
        } else {
            arguments.put("queriesIndexing", cmd.getOptionValue("queriesIndexing"));
        }


        return arguments;

    }


    private static void processIR4SEResults(Map<Integer, Object> queriesResults) throws IOException, InterruptedException, SAXException, ParserConfigurationException {
        String preQueryFilePath = params.get("prequery_file_path");
        Map<Integer, List<String>> fileEntries = FileHReader.readPreQueryFile(preQueryFilePath);
        for (Integer queryId : queriesResults.keySet()) {
            List<Object> queryResults = (List<Object>) queriesResults.get(queryId);
            List<String> tokens = TextHProcessor.getAllWords(TextHProcessor.processText(fileEntries.get(queryId).get(1), null));
            tokens = TextHPreprocessor.addNumbersSpellOut(tokens);
            System.out.println("results for req -----------------------------------------------------------------------------------" + queryId);
            for (Object queryResultObject : queryResults) {
                List<String> queryResult = (ArrayList<String>) queryResultObject;
                processFileByType(queryResult.get(2), tokens, queryId);
            }
            System.out.println("done!");
        }
    }

    private static void processFileByType(String filePath, List<String> slicingCriteria, Integer queryId) throws IOException, InterruptedException, SAXException, ParserConfigurationException {
        String projectDirPath = params.get("project_dir_path");
        String folderPath = params.get("folder_path");
        String projectId = params.get("project_id");
        String fileReplacement = projectDirPath + System.getProperty("file.separator");

        File file = new File(fileReplacement + filePath);

        if (filePath.endsWith(".java")) {
            String slice = JavaHSlicer.getSlice(FileHReader.getFileContent(fileReplacement + filePath), slicingCriteria);
            if (!slice.isEmpty()) {
                Map<String, List<String>> calledMethods = JavaHCallAnalyzer.visitCallNodes(projectDirPath, folderPath, projectId, slice);
                for (String className : calledMethods.keySet()) {
                    List<String> classMethods = calledMethods.get(className);
                    for (String classMethod : classMethods) {
                        slice += "\n--------------------------------\n" + "External Method <\"" + className + "\">\n{" + classMethod + "}\n";
                    }
                }
                FileHWriter.generateFGTHunterResultsFile(folderPath, projectId, slice, file.getName(), FileExtensionEnum.JAVA.getAbbreviation(), queryId);
            }

        } else if (filePath.endsWith(".jsp")) {
            String jspJavaCode = JSPHParser.getFullJavaCodeFromBlockStatements(
                    JSPHParser.generateJavaBlockStatements(
                            JSPHParser.getJavaFragmentsFromSnippets(
                                    JSPHParser.getJSPJavaSnippets(fileReplacement + filePath))));

            String jspJavaCodeSlice = "";
            String javaScriptCodeSlice = "";

            if (!jspJavaCode.trim().isEmpty()) {
                jspJavaCode = "public class dummyFGTHClass {private static void dummyFGHTMethod(){" + jspJavaCode + "}}";
                try {
                    jspJavaCodeSlice = JavaHSlicer.getSlice(
                            jspJavaCode, slicingCriteria);
                    if (!jspJavaCodeSlice.trim().isEmpty()) {
                        jspJavaCodeSlice = "------------------------------------------------------JAVA CODE--------------------------------------\n" + jspJavaCodeSlice + "\n";
                        Map<String, List<String>> calledMethods = JavaHCallAnalyzer.visitCallNodes(projectDirPath, folderPath, projectId, jspJavaCodeSlice);
                        for (String className : calledMethods.keySet()) {
                            List<String> classMethods = calledMethods.get(className);
                            for (String classMethod : classMethods) {
                                jspJavaCodeSlice += "\n--------------------------------\n" + "External Method <\"" + className + "\">\n{" + classMethod + "}\n";
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("conflicto:{");
                    System.out.println(jspJavaCode);
                    System.out.println("}");
                }
            }

            List<String> javaScriptSnippets = JSPHParser.getJSPJavaScriptFragments(fileReplacement + filePath);
            if (!javaScriptSnippets.isEmpty()) {
                String javaScriptCode = "";
                for (String javaScriptSnippet : javaScriptSnippets) {
                    javaScriptCode += javaScriptSnippet;
                }
                if (!javaScriptCode.trim().isEmpty()) {
                    javaScriptCode = "------------------------------------------------------JS CODE---------------------------------------\n" + javaScriptCode + "\n";
                    javaScriptCode = "public class dummyFGTHClass {private static void dummyFGHTMethod(){" + javaScriptCode + "}}";
                    javaScriptCodeSlice = JavaHSlicer.getSlice(javaScriptCode, slicingCriteria);
                }
            }

            if (!javaScriptCodeSlice.trim().isEmpty() || !jspJavaCodeSlice.trim().isEmpty()) {
                FileHWriter.generateFGTHunterResultsFile(folderPath, projectId, "", file.getName(), FileExtensionEnum.JSP.getAbbreviation(), queryId);
                FileHWriter.writeFGTHunterResultsFile(folderPath, projectId, jspJavaCodeSlice, file.getName(), FileExtensionEnum.JSP.getAbbreviation(), queryId);
                FileHWriter.writeFGTHunterResultsFile(folderPath, projectId, javaScriptCodeSlice, file.getName(), FileExtensionEnum.JSP.getAbbreviation(), queryId);
            }

        } else if (filePath.endsWith(".js")) {
            List<String> javaScriptSnippets = JSPHParser.getJSPJavaScriptFragments(fileReplacement + filePath);
            if (!javaScriptSnippets.isEmpty()) {
                String javaScriptCode = "";
                for (String javaScriptSnippet : javaScriptSnippets) {
                    javaScriptCode += javaScriptSnippet;
                }
                if (javaScriptCode.trim().isEmpty()) {
                    String javaScriptCodeSlice = JavaHSlicer.getSlice(javaScriptCode, slicingCriteria);
                    //String javaScriptCodeSlice = javaScriptCode;
                    FileHWriter.generateFGTHunterResultsFile(folderPath, projectId, javaScriptCodeSlice, file.getName(), FileExtensionEnum.JAVASCRIPT.getAbbreviation(), queryId);
                }
            }

        } else if (filePath.endsWith(".xml")) {
            String xmlSlice = XMLHParser.searchTagsByCriteria(fileReplacement + filePath, slicingCriteria);
            FileHWriter.generateFGTHunterResultsFile(folderPath, projectId, xmlSlice, file.getName(), FileExtensionEnum.XML.getAbbreviation(), queryId);
        } else if (filePath.endsWith(".sql")) {
            List<String> sqlSlices = SQLHParser.searchSQLStamentByCriteria(fileReplacement + filePath, slicingCriteria);
            String sqlSlice = "";
            for (String slice : sqlSlices) {
                sqlSlice += slice + "\n";
            }
            FileHWriter.generateFGTHunterResultsFile(folderPath, projectId, sqlSlice, file.getName(), FileExtensionEnum.SQL.getAbbreviation(), queryId);
        } else if (filePath.endsWith(FileExtensionEnum.PROPERTIES.getExtension())) {
            List<String> lineSlices = FileHParser.getFileLinesByCriteria(fileReplacement + filePath, slicingCriteria);
            String fileSlice = "";
            for (String slice : lineSlices) {
                fileSlice += slice + "\n";
            }
            FileHWriter.generateFGTHunterResultsFile(folderPath, projectId, fileSlice, file.getName(), FileExtensionEnum.PROPERTIES.getAbbreviation(), queryId);
        }
    }

    public static void buildProjectQueryFile() throws IOException {
        String folderPath = params.get("folder_path");
        String projectId = params.get("project_id");
        String preQueryFilePath = params.get("prequery_file_path");
        FileHWriter.generateIRF4SEQueryFile(folderPath, projectId);

        Map<Integer, List<String>> fileEntries = FileHReader.readPreQueryFile(preQueryFilePath);

        for (Integer entryId : fileEntries.keySet()) {
            List<String> tokens = TextHProcessor.getAllWords(TextHProcessor.processText(fileEntries.get(entryId).get(1), null));
            tokens = TextHPreprocessor.addNumbersSpellOut(tokens);
            String content = "";
            for (String token : tokens) {
                content += token + " ";
            }
            FileHWriter.addIRF4SEQueryFileEntry(folderPath, projectId, entryId, fileEntries.get(entryId).get(0), content);
        }
    }

    public static void buildProjectCorpusFile() throws IOException, ParserConfigurationException, SAXException, InterruptedException, JSONException {
        String folderPath = params.get("folder_path");
        String projectId = params.get("project_id");
        String projectDirPath = params.get("project_dir_path");

        String fileReplacement = projectDirPath + System.getProperty("file.separator");

        List<String> javaStopWords = FileHReader.readStopWordsFromFile("resources/java_stop_words.txt");
        List<String> javasriptStopWords = FileHReader.readStopWordsFromFile("resources/javascript_stop_words.txt");
        List<String> jspStopWords = new ArrayList<>();
        jspStopWords.addAll(javaStopWords);
        jspStopWords.addAll(javasriptStopWords);

        ArrayList<File> javaFiles = (ArrayList<File>) FileHExplorer.getJavaFilesFromProject(projectDirPath);
        ArrayList<File> javascriptFiles = (ArrayList<File>) FileHExplorer.getJavaScriptFilesFromProject(projectDirPath);
        ArrayList<File> jspFiles = (ArrayList<File>) FileHExplorer.getJSPFilesFromProject(projectDirPath);
        ArrayList<File> sqlFiles = (ArrayList<File>) FileHExplorer.getSQLFilesFromProject(projectDirPath);
        ArrayList<File> xmlFiles = (ArrayList<File>) FileHExplorer.getXMLFilesFromProject(projectDirPath);
        ArrayList<File> propsFiles = (ArrayList<File>) FileHExplorer.getPropsFilesFromProject(projectDirPath);

        //corpus file
        FileHWriter.generateIRF4SECorpusFile(folderPath, projectId);

        //mappings file
        FileHWriter.generateMethodMappingFile(folderPath, projectId);

        //Files processing


        for (File file : sqlFiles) {
            System.out.println(file.getAbsolutePath().replace(fileReplacement, ""));
            List<String> tokens = TextHProcessor.getAllWords(TextHProcessor.processText(SQLHParser.getSQLFileSpeechText(file.getAbsolutePath()), null));
            tokens = TextHPreprocessor.addNumbersSpellOut(tokens);
            String content = "";
            for (String token : tokens) {
                content += token + " ";
            }
            FileHWriter.addIRF4SECorpusFileEntry(folderPath, projectId, file.getAbsolutePath().replace(fileReplacement, ""), content);
        }


        for (File file : xmlFiles) {
            System.out.println(file.getAbsolutePath().replace(fileReplacement, ""));
            List<String> tokens = TextHProcessor.getAllWords(TextHProcessor.processText(XMLHParser.getXMLSpeechTextFromFile(file.getAbsolutePath(), true, true), null));
            tokens = TextHPreprocessor.addNumbersSpellOut(tokens);
            String content = "";
            for (String token : tokens) {
                content += token + " ";
            }
            FileHWriter.addIRF4SECorpusFileEntry(folderPath, projectId, file.getAbsolutePath().replace(fileReplacement, ""), content);
        }


        for (File file : javaFiles) {
            System.out.println(file.getAbsolutePath().replace(fileReplacement, ""));
            //List<String> tokens = TextHPreprocessor.tokenize(JavaHParser.getJavaClassSpeechText(file.getAbsolutePath()));
            List<String> tokens = TextHProcessor.getAllWords(TextHProcessor.processText(SRCMLHParser.getJavaFileSpeechText(file.getAbsolutePath()), javaStopWords));
            FileHWriter.addMethodMappingFileEntry(folderPath, projectId, file.getAbsolutePath().replace(projectDirPath, ""),
                    SRCMLHParser.generateClassMethodMapping(SRCMLHParser.generateSRCMLFromJavaCode(FileHReader.getFileContent(file.getAbsolutePath()))).toString());
            tokens = TextHPreprocessor.addNumbersSpellOut(tokens);
            String content = "";
            for (String token : tokens) {
                content += token + " ";
            }
            FileHWriter.addIRF4SECorpusFileEntry(folderPath, projectId, file.getAbsolutePath().replace(fileReplacement, ""), content);
        }

        for (File file : javascriptFiles) {
            System.out.println(file.getAbsolutePath().replace(fileReplacement, ""));
            List<String> tokens = TextHProcessor.getAllWords(TextHProcessor.processText(JavaScriptHParser.getJavaScriptFileSpeechText(file.getAbsolutePath()), javasriptStopWords));
            tokens = TextHPreprocessor.addNumbersSpellOut(tokens);
            String content = "";
            for (String token : tokens) {
                content += token + " ";
            }
            FileHWriter.addIRF4SECorpusFileEntry(folderPath, projectId, file.getAbsolutePath().replace(fileReplacement, ""), content);
        }

        for (File file : jspFiles) {
            System.out.println(file.getAbsolutePath().replace(fileReplacement, ""));
            List<String> tokens = TextHProcessor.getAllWords(TextHProcessor.processText(JSPHParser.getJSPSpeechText(file.getAbsolutePath()), jspStopWords));
            tokens = TextHPreprocessor.addNumbersSpellOut(tokens);
            String content = "";
            for (String token : tokens) {
                content += token + " ";
            }
            FileHWriter.addIRF4SECorpusFileEntry(folderPath, projectId, file.getAbsolutePath().replace(fileReplacement, ""), content);
        }

        for (File file : propsFiles) {
            System.out.println(file.getAbsolutePath().replace(fileReplacement, ""));
            List<String> tokens = TextHProcessor.getAllWords(TextHProcessor.processText(FileHParser.getFileSpeechText(file.getAbsolutePath()), jspStopWords));
            tokens = TextHPreprocessor.addNumbersSpellOut(tokens);
            String content = "";
            for (String token : tokens) {
                content += token + " ";
            }
            FileHWriter.addIRF4SECorpusFileEntry(folderPath, projectId, file.getAbsolutePath().replace(fileReplacement, ""), content);
        }


    }
}
