package hunter;


import fileio.FileHReader;
import nlp.TextHPreprocessor;
import nlp.TextHProcessor;
import sca.JavaHSlicer;

import java.util.List;
import java.util.Map;

/**
 * Created by mordreth on 3/20/18.
 */
public class Main {


    public static void main(String[] args) throws Exception {

        String filepath = null;

        /*

            filepath = "C:/Users/pcc/Desktop/pruebasFGT/JAVA/0.java";
            System.out.println("----------------------------RESULT JAVA-------------------------");
            System.out.println(JavaHParser.getJavaClassSpeechText(filepath));


            filepath = "C:/Users/pcc/Desktop/pruebasFGT/JSPs/example4.jsp";
            //String filepath = "/Users/mordreth/Projects/Others/Examples/jsp/js.jsp";
            System.out.println("----------------------------RESULT JSP-------------------------");
        System.out.println(JSPHParser.getJSPSpeechText(filepath));

            filepath = "C:/Users/pcc/Desktop/pruebasFGT/SQL/koia_db_v2.2.9.sql";
            System.out.println("----------------------------RESULT SQL-------------------------");
        System.out.println(SQLHParser.getSQLFileSpeechText(filepath));

            filepath = "C:/Users/pcc/Desktop/pruebasFGT/JS/0.js";
            System.out.println("----------------------------RESULT JAVASCRIPT-------------------------");
        System.out.println(JavaScriptHParser.getJavaScriptFileSpeechText(filepath));


        try {
            filepath = "C:/Users/pcc/Desktop/pruebasFGT/XML/0.xml";
            System.out.println("----------------------------RESULT XML-------------------------");
            System.out.println(XMLHParser.getXMLSpeechText(filepath, true, true));
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }

        */

        /*
        filepath = "C:/Users/pcc/Desktop/pruebasFGT/JSP/0.js";
        System.out.println("----------------------------RESULT NL-------------------------");
        //List<String> tokens = TextHPreprocessor.tokenize(FileHParser.getFileSpeechText(filepath));
        //List<String> tokens = TextHPreprocessor.tokenize(JavaHParser.getJavaClassSpeechText(filepath));
        //List<String> tokens = TextHPreprocessor.tokenize(JavaScriptHParser.getJavaScriptFileSpeechText(filepath));
        List<String> tokens = TextHPreprocessor.tokenize(JSPHParser.getJSPSpeechText(filepath));
        //List<String> tokens = TextHPreprocessor.tokenize(JavaHParser.getJavaClassSpeechText(filepath));
        //List<String> tokens = TextHPreprocessor.tokenize(JavaHParser.getJavaClassSpeechText(filepath));

        tokens = TextHPreprocessor.removeBlanks(tokens);
        tokens = TextHPreprocessor.removePunctuation(tokens);
        //tokens = TextHPreprocessor.removeNonLiterals(tokens);
        System.out.println(tokens);
        */

        /*
        ULocale locale = new ULocale(ULocale.ENGLISH.getBaseName());
        Double d = Double.parseDouble("12323");
        NumberFormat formatter = new RuleBasedNumberFormat(locale, RuleBasedNumberFormat.SPELLOUT);
        System.out.println(formatter.format(d));
        */
        /*
        try {
            String forlderPath = "C:\\Users\\pcc\\Desktop\\TESTIR";
            String projectId = "TESTIR-2";
            String projectDirPath = "D:\\desarrollo\\Symplifica\\workspace\\KoiaBackEnd_v2";
            List<String> javaStopWords = FileHReader.readStopWordsFromFile("C:\\Users\\pcc\\IdeaProjects\\fgthunter\\resources\\java_stop_words.txt");
            List<String> javasriptStopWords = FileHReader.readStopWordsFromFile("C:\\Users\\pcc\\IdeaProjects\\fgthunter\\resources\\javascript_stop_words.txt");

            ArrayList<File> javaFiles = (ArrayList<File>) FileHExplorer.getJavaFilesFromProject(projectDirPath);
            ArrayList<File> javascriptFiles = (ArrayList<File>) FileHExplorer.getJavaScriptFilesFromProject(projectDirPath);

            //corpus file
            FileHWriter.generateIRF4SECorpusFile(forlderPath, projectId);

            for (File file : javaFiles) {
                System.out.println(file.getAbsolutePath());
                //List<String> tokens = TextHPreprocessor.tokenize(JavaHParser.getJavaClassSpeechText(file.getAbsolutePath()));
                List<String> tokens = TextHPreprocessor.tokenize(SRCMLHParser.getJavaFileSpeechText(file.getAbsolutePath()));
                tokens = TextHPreprocessor.removeBlanks(tokens);
                tokens = TextHPreprocessor.removePunctuation(tokens);
                tokens = TextHPreprocessor.removeNonLiterals(tokens);
                tokens = TextHPreprocessor.splitIdentifiers(tokens);
                tokens = TextHPreprocessor.removeStopWords(tokens, javaStopWords);
                tokens = TextHPreprocessor.addNumbersSpellOut(tokens);
                String content = "";
                for (String token : tokens) {
                    content += token + " ";
                }
                FileHWriter.addIRF4SECorpusFileEntry(forlderPath, projectId, file.getAbsolutePath().replace(projectDirPath + System.getProperty("file.separator"), ""), content);
            }


            for (File file : javascriptFiles) {
                System.out.println(file.getAbsolutePath());
                List<String> tokens = TextHPreprocessor.tokenize(JavaScriptHParser.getJavaScriptFileSpeechText(file.getAbsolutePath()));
                tokens = TextHPreprocessor.removeBlanks(tokens);
                tokens = TextHPreprocessor.removePunctuation(tokens);
                tokens = TextHPreprocessor.removeNonLiterals(tokens);
                tokens = TextHPreprocessor.splitIdentifiers(tokens);
                tokens = TextHPreprocessor.removeStopWords(tokens, javasriptStopWords);
                tokens = TextHPreprocessor.addNumbersSpellOut(tokens);
                String content = "";
                for (String token : tokens) {
                    content += token + " ";
                }
                FileHWriter.addIRF4SECorpusFileEntry(forlderPath, projectId, file.getAbsolutePath().replace(projectDirPath + System.getProperty("file.separator"), ""), content);
            }

        } catch (SAXException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        */

        //List<String> locs = new ArrayList<>();
        //locs.add("352");
        //locs.add("632");

        System.out.println("-------------------------------------------------------------------------------");
        //System.out.println(SRCMLHParser.getLinesOfcode("C:/Users/pcc/Desktop/pruebasFGT/JAVA/0.xml", locs));

        //System.out.println(SRCMLHParser.getCallGraph(FileHReader.getFileContent("C:/Users/pcc/Desktop/pruebasFGT/JAVA/0.java")));

        //System.out.println(SRCMLHParser.getCallGraph(FileHReader.getFileContent("C:/Users/pcc/Desktop/pruebasFGT/JAVA/0.java")));

        //System.out.println(SRCMLHParser.generateClassMethodMapping("C:/Users/pcc/Desktop/pruebasFGT/JAVA/0.xml"));

        //String javaCode = JavaHSlicer.getSlice(FileHReader.getFileContent("C:/Users/pcc/Desktop/pruebasFGT/JAVA/0.java"), "varNAme");
        //System.out.println(javaCode);

        //System.out.println(JavaHCallAnalyzer.visitCallNodes(projectDirPath,folderPath, projectId, FileHReader.getFileContent("D:\\desarrollo\\CREDITCORP\\Divisas\\Divisas\\src\\com\\osmosyscol\\divisas\\servlet\\DescargarArchivoSoporteServlet.java")));
        //System.out.println(SRCMLHParser.getClassMethodBySignature("D:\\desarrollo\\CREDITCORP\\Divisas\\Divisas\\src\\com\\osmosyscol\\divisas\\logica\\servicios\\CargueInfoAduaneraServicio.java","cargarCamposDesdeExcel"));

        /*
        Map<String, String> params = new HashMap<>();
        params.put("ret_model", "LUCENE");
        params.put("system", "TESTIR-4");
        params.put("base_dir", "C:\\Users\\pcc\\Desktop\\TESTIR-4");
        params.put("name_config", "TESTIR-4");

        params.put("queries_dir", "C:\\Users\\pcc\\Desktop\\TESTIR-4");
        params.put("prequery_file_path", "C:\\Users\\pcc\\Desktop\\pruebasFGT\\TEMP\\example.csv");
        params.put("folder_path", "C:\\Users\\pcc\\Desktop\\TESTIR-4");
        params.put("project_id", "TESTIR");
        params.put("project_dir_path", "D:\\desarrollo\\Symplifica\\workspace\\KoiaBackEnd_v2");

        String folderPath = params.get("folder_path");
        String projectId = params.get("project_id");
        String preQueryFilePath = params.get("prequery_file_path");
        FileHWriter.generateIRF4SEQueryFile(folderPath, projectId);

        Map<Integer, List<String>> fileEntries = FileHReader.readPreQueryFile(preQueryFilePath);

        for (Integer entryId : fileEntries.keySet()) {
            List<String> tokens = TextHPreprocessor.tokenize(fileEntries.get(entryId).get(1));
            tokens = TextHPreprocessor.removeBlanks(tokens);
            tokens = TextHPreprocessor.removePunctuation(tokens);
            tokens = TextHPreprocessor.removeNonLiterals(tokens);
            tokens = TextHPreprocessor.splitIdentifiers(tokens);
            tokens = TextHPreprocessor.addNumbersSpellOut(tokens);
            String content = "";
            for (String token : tokens) {
                content += token + " ";
            }
            FileHWriter.addIRF4SEQueryFileEntry(folderPath, projectId, entryId + 1, fileEntries.get(entryId).get(0), content);
        }

        RetrievalHProcessor processor = new RetrievalHProcessor();
        processor.processSystem(params);

        */

        /*

        Map<String, String> params = new HashMap<>();
        params.put("folder_path", "C:\\Users\\pcc\\Desktop\\KoiaFG");
        params.put("project_id", "KoiaFG");
        params.put("project_dir_path", "D:\\desarrollo\\Symplifica\\workspace\\KoiaBackEnd_v2");
        params.put("prequery_file_path", "C:\\Users\\pcc\\Desktop\\pruebasFGT\\TEMP\\example.csv");

        params.put("ret_model", "LUCENE");
        params.put("system", "KoiaFG");
        params.put("base_dir", "C:\\Users\\pcc\\Desktop");
        params.put("name_config", "KoiaFG");
        params.put("queries_dir", "C:\\Users\\pcc\\Desktop");

        Map<Integer, Object> irResults = FileHReader.readIR4SEResults(params.get("base_dir")
                + System.getProperty("file.separator") + params.get("system") +
                System.getProperty("file.separator") + params.get("system") + ParameterUtils.ir4se_resultsFileExt);

        for (Integer queryId : irResults.keySet()) {
            System.out.println(queryId);
        }
        */

        //System.out.println(JSPHParser.getJSPSpeechText("C:/Users/pcc/Desktop/pruebasFGT/JSPs/example4.jsp"));
        //System.out.println(JSPHParser.getJSPFullText("C:/Users/pcc/Desktop/pruebasFGT/JSPs/example1.jsp"));
        //System.out.println(JSPHParser.getJSPJavaCode("C:/Users/pcc/Desktop/pruebasFGT/JSPs/example1.jsp"));
        //System.out.println(JSPHParser.getJSPJavaScriptCode("C:/Users/pcc/Desktop/pruebasFGT/JSPs/example1.jsp"));

        //System.out.println(JSPHParser.getJavaFragmentsFromSnippets(JSPHParser.getJSPJavaSnippets("C:/Users/pcc/Desktop/pruebasFGT/JSPs/example4.jsp")));
        //System.out.println(JSPHParser.getJSPFullText("C:/Users/pcc/Desktop/pruebasFGT/JSPs/example4.jsp"));
        //System.out.println(JSPHParser.getFullJavaCodeFromBlockStatements(JSPHParser.generateJavaBlockStatements(JSPHParser.getJavaFragmentsFromSnippets(JSPHParser.getJSPJavaSnippets("C:/Users/pcc/Desktop/pruebasFGT/JSPs/example4.jsp")))));
        //System.out.println(JSPHParser.getFullJavaCodeFromBlockStatements(JSPHParser.generateJavaBlockStatements(JSPHParser.getJavaFragmentsFromSnippets(JSPHParser.getJSPJavaSnippets("C:/Users/pcc/Desktop/pruebasFGT/JSPs/example4.jsp")))));
        // System.out.println(JSPHParser.getJSPSpeechText("C:/Users/pcc/Desktop/pruebasFGT/JSPs/example1.jsp"));
        //System.out.println(FileHReader.getFileContent("C:\\Users\\pcc\\Desktop\\pruebasFGT\\TEMP\\temp.java"));
        //System.out.println(SRCMLHParser.getJavaFileSpeechText(FileHWriter.generateTempJavaFile("C:\\Users\\pcc\\Desktop\\pruebasFGT\\temp.java")));

        //System.out.println(JavaScriptHParser.getJavaScriptFileSpeechText("C:/Users/pcc/Desktop/pruebasFGT/JS/0.js"));
        //System.out.println(JavaScriptHParser.getJavaScriptFileSpeechText("D:\\desarrollo\\CREDITCORP\\Divisas\\Divisas\\WebContent\\resources\\scripts\\confirmarOperaciones\\43.js"));
        //System.out.println(JSPHParser.getJSPSpeechText("C:/Users/pcc/Desktop/pruebasFGT/JSPs/example1.jsp"));


        /*
        List<String> javaStopWords = FileHReader.readStopWordsFromFile("resources/java_stop_words.txt");

        Map<Integer, List<String>> fileEntries = FileHReader.readPreQueryFile("C:\\Users\\pcc\\Desktop\\pruebasFGT\\TEMP\\example.csv");
        for (Integer index : fileEntries.keySet()) {
            List<String> tokens = TextHProcessor.getAllWords(TextHProcessor.processText(fileEntries.get(index).get(1), javaStopWords));
            tokens = TextHPreprocessor.addNumbersSpellOut(tokens);
            System.out.println(tokens);
            //System.out.println(TextHProcessor.getAllTokens(TextHProcessor.processText(fileEntries.get(index).get(1), javaStopWords)));
        }
        */


        Map<Integer, List<String>> fileEntries = FileHReader.readPreQueryFile("/Users/mordreth/Downloads/Temp/iTrust.csv");
        List<String> tokens = TextHProcessor.getAllWords(TextHProcessor.processText(fileEntries.get(1).get(1), null));
        tokens = TextHPreprocessor.addNumbersSpellOut(tokens);
        System.out.println("---------------------------------CRITERIA ----------------------------------");
        System.out.println(tokens);
        String slice = JavaHSlicer.getSlice(FileHReader.getFileContent("/Users/mordreth/Downloads/Temp/0.java"), tokens);
        if(!slice.isEmpty()){
            System.out.println("-------------------------------RESULT ---------------------------------");
            System.out.println(slice);
        }




        /*

        List<String> javaStopWords = FileHReader.readStopWordsFromFile("resources/java_stop_words.txt");
        List<String> javasriptStopWords = FileHReader.readStopWordsFromFile("resources/javascript_stop_words.txt");
        List<String> jspStopWords = new ArrayList<>();
        jspStopWords.addAll(javaStopWords);
        jspStopWords.addAll(javasriptStopWords);

        List<String> tokens = TextHProcessor.getAllWords(TextHProcessor.processText(JSPHParser.getJSPSpeechText("C:\\Users\\pcc\\Desktop\\pruebasFGT\\JSPs\\error.jsp"), jspStopWords));
        tokens = TextHPreprocessor.addNumbersSpellOut(tokens);
        String content = "";
        for (String token : tokens) {
            content += token + " ";
        }

        System.out.println(content);

        */

        //System.out.println(JSPHParser.getJSPFullText("C:\\Users\\pcc\\Desktop\\pruebasFGT\\JSPs\\error.jsp"));

        //System.out.println(JSPHParser.getJSPSpeechText("C:\\Users\\pcc\\Desktop\\pruebasFGT\\JSPs\\error.jsp"));
    }
}
