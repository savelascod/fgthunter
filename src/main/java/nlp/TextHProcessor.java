package nlp;

import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreAnnotations.*;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;
import nlp.entity.Sentence;
import nlp.entity.Token;
import utils.TextUtils;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;

public class TextHProcessor {

    public static final String[] PARENTHESIS = {"-LCB-", "-RCB-", "-LRB-", "-RRB-", "-LSB-", "-RSB-"};
    public static final String[] PARENTHESIS2 = {"LCB", "RCB", "LRB", "RRB", "LSB", "RSB"};
    private static final String SPACE = " ";
    private static StanfordCoreNLP defaultPipeline;
    private static HashMap<String, String> POS_TAGS = new HashMap<>();

    static {

        POS_TAGS.put("JJ", "JJ");
        POS_TAGS.put("JJR", "JJ");
        POS_TAGS.put("JJS", "JJ");

        POS_TAGS.put("NN", "NN");
        POS_TAGS.put("NNS", "NN");
        POS_TAGS.put("NNP", "NN");
        POS_TAGS.put("NNPS", "NN");

        POS_TAGS.put("PRP", "PRP");
        POS_TAGS.put("PRP$", "PRP");

        POS_TAGS.put("RB", "RB");
        POS_TAGS.put("RBR", "RB");
        POS_TAGS.put("RBS", "RB");

        POS_TAGS.put("VB", "VB");
        POS_TAGS.put("VBD", "VB");
        POS_TAGS.put("VBG", "VB");
        POS_TAGS.put("VBN", "VB");
        POS_TAGS.put("VBP", "VB");
        POS_TAGS.put("VBZ", "VB");

        POS_TAGS.put("WDT", "WH");
        POS_TAGS.put("WP", "WH");
        POS_TAGS.put("WP$", "WH");
        POS_TAGS.put("WRB", "WH");

        POS_TAGS.put("CC", "CC");

        POS_TAGS.put("CD", "CD");
    }

    private synchronized static void initDefaultPipeline() {

        if (defaultPipeline != null) {
            return;
        }

        Properties props = new Properties();
        props.setProperty("annotators", "tokenize, ssplit, pos, lemma");
        props.setProperty("tokenize.options", "untokenizable=noneKeep,invertible=true");
        defaultPipeline = new StanfordCoreNLP(props);
    }

    public static String getGeneralPos(String pos) {
        String tag = POS_TAGS.get(pos);
        if (tag != null) {
            return tag;
        }
        return pos;
    }

    public static List<Sentence> processText(String text, List<String> stopWords) {

        List<Sentence> parsedSentences = new ArrayList<>();
        if (text == null)
            return parsedSentences;

        initDefaultPipeline();

        Annotation document = new Annotation(text);
        defaultPipeline.annotate(document);
        List<CoreMap> sentences = document.get(SentencesAnnotation.class);

        Integer id = 0;

        for (CoreMap sentence : sentences) {

            List<CoreLabel> tokenList = sentence.get(TokensAnnotation.class);

            String sentenceText = sentence.get(CoreAnnotations.TextAnnotation.class);
            Sentence parsedSentence = new Sentence(id.toString(), sentenceText);

            for (CoreLabel token : tokenList) {
                splitCamelCaseAndAddTokens(stopWords, parsedSentence, token);
                //addToken(stopWords, parsedSentence, token);
            }

            if (parsedSentence.isEmpty()) {
                continue;
            }

            parsedSentences.add(parsedSentence);
            id++;
        }

        return parsedSentences;
    }


    private static void splitCamelCaseAndAddTokens(List<String> stopWords,
                                                   Sentence parsedSentence, CoreLabel token) {

        String word = token.get(TextAnnotation.class);
        String tokenCC = TextUtils.splitByCharacterTypeCamelCase(word);

        Annotation tokenAnnot = new Annotation(tokenCC);
        defaultPipeline.annotate(tokenAnnot);

        List<CoreMap> sentences = tokenAnnot.get(SentencesAnnotation.class);
        for (CoreMap sentence : sentences) {

            List<CoreLabel> tokenList = sentence.get(TokensAnnotation.class);
            for (CoreLabel newToken : tokenList) {
                addToken(stopWords, parsedSentence, newToken);
            }
        }
    }

    private static void addToken(List<String> stopWords, Sentence parsedSentence,
                                 CoreLabel token) {
        String word = token.get(TextAnnotation.class);
        String lemma = token.get(LemmaAnnotation.class).toLowerCase();
        String pos = token.get(PartOfSpeechAnnotation.class);
        String generalPos = getGeneralPos(pos);

        if (isPunctuation(lemma)) {
            return;
        }

        // filters
        if (!matchesPOS(generalPos, "NN")
                && !matchesPOS(generalPos, "VB")
                && !matchesPOS(generalPos, "CD")
                && !matchesPOS(generalPos, "JJ")) {
            return;
        }


        if (isShortTerm(lemma, pos, 3)) {
            return;
        }


        if (stopWords != null && stopWords.size() > 0 && isStopWord(stopWords, lemma, pos)) {
            return;
        }


        String stem = GeneralStemmer.stemmingPorter(word).toLowerCase();

        Token parsedToken = new Token(word, generalPos, pos, lemma, stem);
        parsedSentence.addToken(parsedToken);
    }

    public static boolean isShortTerm(String lemma, String pos, int length) {
        if (matchesPOS(pos, "cd")) {
            return false;
        }
        return (lemma.length() < length);
    }

    public static boolean matchesPOS(String pos, String posToMatch) {
        return posToMatch.equalsIgnoreCase(pos);
    }

    public static boolean isStopWord(List<String> stopWords, String lemma, String pos) {
        return stopWords.contains(lemma);
    }

    public static boolean containsSpecialChars(String str, String pos) {

        if (matchesPOS(pos, "sym")) {
            return true;
        }

        if ("'s".equalsIgnoreCase(str)) {
            return false;
        }

        if ("'ll".equalsIgnoreCase(str)) {
            return false;
        }

        String[] split = str.split("/");
        if (split.length > 1) {

            for (String s : split) {
                if (isSpecialChar(s)) {
                    return true;
                }
            }

            return pos.equalsIgnoreCase("cd");

        }

        split = str.split("-");
        if (split.length > 1) {

            for (String s : split) {
                if (isSpecialChar(s)) {
                    return true;
                }
            }

            return pos.equalsIgnoreCase("cd");

        }

        boolean b = isSpecialChar(str);
        return b;
    }

    public static boolean isSpecialChar(String str) {
        String[] split = str.split("[^a-zA-Z0-9]");
        boolean b = split.length != 1;
        return b;
    }

    public static boolean isNumber(String token) {
        boolean isInt = token.matches("\\d+((\\h|\\s)+\\d+)+");
        if (isInt) {
            return true;
        }

        try {
            Double.valueOf(token);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }

    }

    public static boolean isPunctuation(String token) {
        return token.matches("[\\p{P}\\p{S}]") || isParenthesis(token);
    }

    public static boolean isParenthesis(String token) {
        for (String parenthesis : PARENTHESIS) {
            if (token.toLowerCase().contains(parenthesis.toLowerCase())) {
                return true;
            }
        }

        for (String parenthesis : PARENTHESIS2) {
            if (token.toLowerCase().equals(parenthesis.toLowerCase())) {
                return true;
            }
        }
        return false;
    }

    public static String getStringFromSentences(List<Sentence> sentences) {
        StringBuffer buffer = new StringBuffer();
        for (Sentence sentence : sentences) {
            List<Token> tokens = sentence.getTokens();
            for (Token token : tokens) {
                buffer.append(token.getLemma());
                buffer.append(SPACE);
            }
        }
        return buffer.toString().trim();
    }

    public static List<Token> getAllTokens(List<Sentence> sentences) {
        List<Token> tokens = new ArrayList<>();
        for (Sentence sentence : sentences) {
            tokens.addAll(sentence.getTokens());
        }
        return tokens;
    }

    public static List<String> getAllWords(List<Sentence> sentences) {
        List<String> words = new ArrayList<>();
        for (Sentence sentence : sentences) {
            for (Token token : sentence.getTokens()) {
                words.add(token.getWord());
            }
        }
        return words;
    }

    public static List<Token> getUniqueTokensBy(List<Sentence> sentences, Function<Token, Object> fieldFn) {

        List<Token> tokens = TextHProcessor.getAllTokens(sentences);
        // unique terms by lemma
        List<Token> uniqueTokens = getUniqueTokensBy(fieldFn, tokens);

        return uniqueTokens;
    }

    private static List<Token> getUniqueTokensBy(Function<Token, Object> fieldFn, List<Token> tokens) {
        List<Token> uniqueTokens = new ArrayList<>();
        tokens.stream().filter(distinctByField(fieldFn)).forEach(t -> {
            uniqueTokens.add(t);
        });
        return uniqueTokens;
    }

    private static <T> Predicate<T> distinctByField(Function<? super T, Object> fieldExtractor) {
        Map<Object, Boolean> elemsSeen = new ConcurrentHashMap<>();
        return t -> elemsSeen.putIfAbsent(fieldExtractor.apply(t), Boolean.TRUE) == null;
    }

    public static List<Sentence> getSentencesWithUniqueTerms(List<Sentence> sentences,
                                                             Function<Token, Object> fieldFn) {

        List<Sentence> uniqueSentences = new ArrayList<>();
        sentences.stream().forEach(s -> {
            List<Token> unTkns = getUniqueTokensBy(fieldFn, s.getTokens());
            uniqueSentences.add(new Sentence(s.getId(), unTkns, s.getText()));
        });

        return uniqueSentences;

    }

    public static String getStringFromLemmas(Sentence sentence) {
        List<Token> tokens = sentence.getTokens();
        return getStringFromLemmas(tokens);
    }

    public static String getStringFromTerms(Sentence sentence) {
        List<Token> tokens = sentence.getTokens();
        return getStringFromTerms(tokens);
    }

    public static String getStringFromTerms(List<Token> tokens) {
        StringBuffer buffer = new StringBuffer();
        for (Token token : tokens) {
            buffer.append(token.getWord());
            buffer.append(SPACE);
        }
        return buffer.toString().trim();
    }

    public static String getStringFromTermsAndPos(Sentence sentence, boolean lowercase
                                                  // , boolean processParenthesis
    ) {
        StringBuffer buffer = new StringBuffer();
        List<Token> tokens = sentence.getTokens();
        for (Token token : tokens) {
            String word = token.getWord();

            // ----------------------------------

            if (lowercase) {
                word = word.toLowerCase();
            }

            // ----------------------------------

            // if (processParenthesis) {
            // }

            // ----------------------------------

            buffer.append(word);
            buffer.append("$$");
            buffer.append(token.getPos());
            buffer.append(SPACE);
        }
        return buffer.toString().trim();
    }

    public static String getStringFromLemmasAndPos(Sentence sentence) {
        StringBuffer buffer = new StringBuffer();
        List<Token> tokens = sentence.getTokens();
        for (Token token : tokens) {
            buffer.append(token.getLemma());
            buffer.append("$$");
            buffer.append(token.getPos());
            buffer.append(SPACE);
        }
        return buffer.toString().trim();
    }

    public static boolean checkGeneralPos(String tag, String... tagsToAssert) {
        String gnrlPos = getGeneralPos(tag);
        return Arrays.stream(tagsToAssert).anyMatch(t -> t.equals(gnrlPos));
    }

    public static String getStringFromLemmas(List<Token> tokens) {
        StringBuffer buffer = new StringBuffer();
        for (Token token : tokens) {
            buffer.append(token.getLemma());
            buffer.append(SPACE);
        }
        return buffer.toString().trim();
    }
}
