package nlp;

import com.ibm.icu.text.NumberFormat;
import com.ibm.icu.text.RuleBasedNumberFormat;
import com.ibm.icu.util.ULocale;
import edu.stanford.nlp.ling.CoreAnnotations.LemmaAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TextAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;
import org.apache.commons.lang3.StringUtils;

import java.util.*;

public class TextHPreprocessor {
    private static final String[] PARENTHESIS = {"-LCB-", "-RCB-", "-LRB-", "-RRB-", "-LSB-", "-RSB-"};
    private static StanfordCoreNLP pipeline;
    private static StanfordCoreNLP pipelineLemma;

    private static HashMap<String, String> POS_TAGS = new HashMap<>();

    static {

        POS_TAGS.put("JJ", "JJ");
        POS_TAGS.put("JJR", "JJ");
        POS_TAGS.put("JJS", "JJ");

        POS_TAGS.put("NN", "NN");
        POS_TAGS.put("NNS", "NN");
        POS_TAGS.put("NNP", "NN");
        POS_TAGS.put("NNPS", "NN");

        POS_TAGS.put("PRP", "PRP");
        POS_TAGS.put("PRP$", "PRP");

        POS_TAGS.put("RB", "RB");
        POS_TAGS.put("RBR", "RB");
        POS_TAGS.put("RBS", "RB");

        POS_TAGS.put("VB", "VB");
        POS_TAGS.put("VBD", "VB");
        POS_TAGS.put("VBG", "VB");
        POS_TAGS.put("VBN", "VB");
        POS_TAGS.put("VBP", "VB");
        POS_TAGS.put("VBZ", "VB");

        POS_TAGS.put("WDT", "WH");
        POS_TAGS.put("WP", "WH");
        POS_TAGS.put("WP$", "WH");
        POS_TAGS.put("WRB", "WH");
    }

    static {
        Properties props = new Properties();
        props.setProperty("annotators", "tokenize, ssplit");
        pipeline = new StanfordCoreNLP(props);

        props = new Properties();
        props.setProperty("annotators", "tokenize, ssplit, pos, lemma");
        pipelineLemma = new StanfordCoreNLP(props);
    }

    public static List<String> tokenize(String text) {
        Annotation document = new Annotation(text);
        pipeline.annotate(document);

        List<String> tokens = new ArrayList<>();

        List<CoreMap> sentences = document.get(SentencesAnnotation.class);
        for (CoreMap sentence : sentences) {
            for (CoreLabel token : sentence.get(TokensAnnotation.class)) {
                String word = token.get(TextAnnotation.class);
                tokens.add(word);
            }
        }

        return tokens;
    }

    public static List<String> removeNonLiterals(List<String> tokens) {
        List<String> validTokens = new ArrayList<>();
        for (String token : tokens) {
            String tokenNoLiterals = token.replaceAll("[^a-zA-Z0-9]", " ");
            List<String> newTokens = tokenize(tokenNoLiterals);
            validTokens.addAll(newTokens);
        }
        return validTokens;
    }

    public static List<String> removeIntegers(List<String> tokens) {
        List<String> validTokens = new ArrayList<>();
        for (String token : tokens) {
            if (!token.matches("\\d+")) {
                validTokens.add(token);
            }
        }
        return validTokens;
    }

    public static List<String> addNumbersSpellOut(List<String> tokens) {
        List<String> convertedTokens = new ArrayList<>();
        ULocale locale = new ULocale(ULocale.ENGLISH.getBaseName());
        for (String token : tokens) {
            convertedTokens.add(token);
            if (token.matches("\\d+")) {
                Double d = Double.parseDouble(token);
                NumberFormat formatter = new RuleBasedNumberFormat(locale, RuleBasedNumberFormat.SPELLOUT);
                convertedTokens.add(formatter.format(d));
            }
        }
        return convertedTokens;
    }

    public static List<String> splitIdentifiers(List<String> tokens) {
        List<String> validTokens = new ArrayList<>();
        for (String token : tokens) {
            String[] tokenSplit = StringUtils.splitByCharacterTypeCamelCase(token);
            validTokens.addAll(Arrays.asList(tokenSplit));
        }
        return validTokens;
    }

    public static List<String> removeStopWords(List<String> tokens, List<String> stopWords) {
        List<String> validTokens = new ArrayList<>();
        for (String token : tokens) {
            if (!isStopWord(token, stopWords)) {
                validTokens.add(token);
            }
        }
        return validTokens;
    }

    private static boolean isStopWord(String token, List<String> stopWords) {
        for (String stopWord : stopWords) {
            if (stopWord.equalsIgnoreCase(token)) {
                return true;
            }
        }
        return false;
    }

    public static List<String> toLowerCase(List<String> tokens) {
        List<String> validTokens = new ArrayList<>();
        for (String token : tokens) {
            validTokens.add(token.toLowerCase());
        }
        return validTokens;
    }

    public static List<String> tokenizeToLemma(String text) {
        Annotation document = new Annotation(text);
        pipelineLemma.annotate(document);

        List<String> tokens = new ArrayList<>();

        List<CoreMap> sentences = document.get(SentencesAnnotation.class);
        for (CoreMap sentence : sentences) {
            for (CoreLabel token : sentence.get(TokensAnnotation.class)) {
                String lemma = token.get(LemmaAnnotation.class);
                tokens.add(lemma);
            }
        }

        return tokens;
    }

    public static List<String> removePunctuation(List<String> tokens) {
        List<String> validTokens = new ArrayList<>();
        for (String token : tokens) {
            if (!isPunctuation(token)) {
                validTokens.add(token);
            }
        }
        return validTokens;
    }

    private static boolean isPunctuation(String token) {
        return token.matches("[\\p{P}\\p{S}]") || isParenthesis(token);
    }

    public static boolean isParenthesis(String token) {
        for (String parenthesis : PARENTHESIS) {
            if (token.contains(parenthesis)) {
                return true;
            }
        }
        return false;
    }

    public static List<String> removeBlanks(List<String> tokens) {
        List<String> validTokens = new ArrayList<>();
        for (String token : tokens) {
            if (!token.matches("[\\p{Space}]+") && !token.trim().isEmpty()) {
                validTokens.add(token);
            }
        }
        return validTokens;
    }

    public static List<String> removeShortTokens(List<String> tokens, int size) {
        List<String> validTokens = new ArrayList<>();
        for (String token : tokens) {
            if (token.length() > size) {
                validTokens.add(token);
            }
        }
        return validTokens;
    }

    public static List<String> removeSpecifiedTokens(List<String> tokens, Set<String> tokensToRemove) {
        List<String> validTokens = new ArrayList<>();
        for (String token : tokens) {
            if (!isTokenInList(token, tokensToRemove)) {
                validTokens.add(token);
            }
        }
        return validTokens;
    }

    private static boolean isTokenInList(String token, Set<String> tokensToRemove) {
        for (String t : tokensToRemove) {
            if (t.equalsIgnoreCase(token)) {
                return true;
            }
        }
        return false;
    }

    public static List<List<String>> tokenizeInSentences(String text) {
        Annotation document = new Annotation(text);
        pipeline.annotate(document);

        List<List<String>> sentencesTokens = new ArrayList<>();

        List<CoreMap> sentences = document.get(SentencesAnnotation.class);
        for (CoreMap sentence : sentences) {

            List<String> tokens = new ArrayList<>();
            for (CoreLabel token : sentence.get(TokensAnnotation.class)) {
                String word = token.get(TextAnnotation.class);
                tokens.add(word);
            }

            sentencesTokens.add(tokens);
        }

        return sentencesTokens;
    }

    public static String getGeneralPos(String pos) {
        String tag = POS_TAGS.get(pos);
        if (tag != null) {
            return tag;
        }
        return pos;
    }
}
